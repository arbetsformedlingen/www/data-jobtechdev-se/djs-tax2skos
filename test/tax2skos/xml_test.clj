(ns tax2skos.xml-test
  (:require [clojure.test :refer :all]
            [tax2skos.xml :refer :all]
            [clojure.data.xml :as xml]
            [clojure.java.io :as io])
  (:import [java.io File]))

(deftest various-xml-tests
  (is (= (xml-sort-feature {:tag :prefLabel,
                            :attrs #:xml{:lang "sv"},
                            :content '("Flytande")})
         [:prefLabel :xml/lang "sv" "Flytande"]))
  (let [data0 (-> "sampledata/language-level.v1.xml" slurp xml/parse-str)
        data1 (-> "sampledata/language-level.v2.xml" slurp xml/parse-str)
        sdata0 (normalize-xml-recursively data0)
        sdata1 (normalize-xml-recursively data1)
        file (File/createTempFile "mjao" ".xml")]
    (is (not= data0 data1))
    (is (not= data0 sdata0))
    (is (not= data1 sdata1))
    (is (= sdata0 sdata1))
    (println "File" (str file))
    (spit file (slurp "sampledata/language-level.v1.xml"))
    (normalize-xml-file-in-place file)
    (is (= sdata0 (read-xml file)))))
