(ns tax2skos.graphql-request-test
  (:require [tax2skos.graphql-request :refer :all]
            [clojure.string :as cljstr]
            [clojure.test :refer :all]))

(deftest query-result-selection-name-test
  (is (= (unpack-query-selection-results {:data {:concepts [:mjao]}})
         [:concepts [:mjao]])))

(defn run-query-test []
  (let [qs "query MyQuery {
  concepts(preferred_label_contains: \"kare\") {
    id
    preferred_label
  }
}
"
        x (run-query qs default-config)
        y (run-parameterized-query qs default-config)
        z (run-parameterized-query qs (merge default-config {:page-size 50}))
        ]
    (assert (is (= x y)))
    (assert (is (= x z)))))
