(ns tax2skos.graphql-test
  (:require [clojure.test :refer :all]
            [tax2skos.graphql :refer :all]
            [clojure.string :as cljstr]))

(def ex0 "query MyQuery {
  concepts(type: \"occupation-name\") {
    id
  }
}
")

(def ex3 "query MyQuery {
  concepts(type: \"occupation-name\", offset: 10, limit: 10) {
    id
  }
}
")

(def ex4 "query MyQuery {   concepts {alternative_labels esco_uri definition eures_code_2014 eures_nace_code_2007 hidden_labels id preferred_label short_description ssyk_code_2012 type uri broad_match {id} broader {id} close_match {id} exact_match {id} narrow_match {id} narrower {id} possible_combinations {id} related {id} replaced_by {id} replaces {id} substituted_by {id} substitutes {id} unlikely_combinations {id}}}")


(deftest graphql-parsing-and-manipulation-test
  (let [doc (parse ex0)
        updated (provide-query-selection-arguments doc {"concepts" {"offset" 113,
                                                                    "limit" 51,
                                                                    "version" "kobra"
                                                                    "ekskog" nil}
                                                        "granskog" {"offset" 34}
                                                        "tallskog" {"offset" 119}
                                                        "bokskog" {"offset" 98234}})
        s (render-compact updated)]
    (is doc)
    (is (string? s))
    (is (cljstr/includes? s "113"))
    (is (cljstr/includes? s "51"))
    (is (cljstr/includes? s "kobra"))
    (is (not (cljstr/includes? s "ekskog")))
    (is (not (cljstr/includes? s "granskog")))
    (is (not (cljstr/includes? s "tallskog")))
    (is (not (cljstr/includes? s "bokskog")))
    (is (not (cljstr/includes? s "34")))
    (is (not (cljstr/includes? s "119")))
    (is (not (cljstr/includes? s "98234")))
    (is (-> s parse render-compact (= s)))))

(deftest empty-args-test
  (let [doc (parse ex4)]
    (is (= (render-compact doc)
           (render-compact (provide-query-selection-arguments doc {"concepts" {}}))))))

(deftest parsable-test
  (is (parsable? "query MyQuery {id}"))
  (is (not (parsable? "query MyQuery {")))
  (is (not (parsable? 119))))
