(ns tax2skos.utils-test
  (:require [clojure.test :refer :all]
            [tax2skos.utils :refer :all]
            [clojure.java.io :as io]))

(deftest test-change-path-root
  (let [src-root (io/file "/tmp/katt")
        dst-root (io/file "/home/ubuntu")
        src-file (io/file "/tmp/katt/prog/Makefile")
        dst-file (io/file "/home/ubuntu/prog/Makefile")]
    (is (str dst-file)
        (str (change-path-root src-root dst-root src-file)))))

(deftest merge-into-test
  (is (= {:a "abc" :b 3} (merge-into-structure class {:a "123" :b 3} {:a "abc"})))
  (is (= "abc" (merge-into-structure class "123" "abc")))
  (is (thrown? clojure.lang.ExceptionInfo (merge-into-structure class {:a 123 :b 3} {:a "abc"})))
  (is (= (merge-into-structure class {:a {:b "c"} :e :f} {:a {:b "d"}})
         {:a {:b "d"}, :e :f})))

(deftest sort-all-maps-test
  (->> [[[{:b 3 :a 4}]]]
       sort-all-unordered
       first first first first
       (= [:a 4])
       is)
  (->> [[["z" "x" "y"]]]
       sort-all-unordered
       (= [[["x" "y" "z"]]])
       is)
  (->> {:c 3 :a [[{:id "k"} {:id "a"}]]}
       sort-all-unordered
       vec
       (= [[:a [[{:id "a"} {:id "k"}]]]
           [:c 3]])
       is))

(deftest clean-strings-test
  (is (= {:a [[["    abc        123    "]]]}
         (clean-strings-recursively remove-line-breaks {:a [[["\n\r\n\rabc\n\r\n\r\n\r\n\r123\n\r\n\r"]]]}))))

(deftest markdown-utils-test
  (is (= "[a](b)" (md-link "a" "b")))
  (is (= "a\nb\nc" (flatten-lines ["a" [[[[[[[["b"]]]] "c"]]]]]))))
