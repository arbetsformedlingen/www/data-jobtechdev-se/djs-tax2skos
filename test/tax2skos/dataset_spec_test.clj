(ns tax2skos.dataset-spec-test
  (:require [tax2skos.dataset-spec :as dataset-spec]
            [clojure.test :refer [deftest is]]))


(deftest import-csv-spec-test
  (let [data (dataset-spec/import-csv-spec "taxonomy-dataset-3.csv")]
    (is (= [":dcat:Dataset \"sv\""
            ":dcat:Dataset \"en\""
            ":dcterms:description \"sv\""
            ":dcterms:description \"en\""
            ":dcat:keyword"
            ":dcterms:creator (anges om extern producent)"
            ":dcterms:accrualPeriodicity"
            ":dcat:landingPage"
            ":type (hos oss)"
            ":query"]
           (mapv str (:columns data))))
    (is (seq (:data data)))
    (is (every? map? (:data data)))))
