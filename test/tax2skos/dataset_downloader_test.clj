(ns tax2skos.dataset-downloader-test
  (:require [clojure.test :refer :all]
            [tax2skos.dataset-downloader :refer :all]
            [clojure.string :as cljstr])
  (:import [java.net URI]))

(deftest sanitization-test
  (is (= [[] {:desc-sv "En ganska användbar datamängd."}]
         (sanitize-values
          empty-string-message
          {:desc-sv
           ["En ganska användbar datamängd."
            "Beskrivning saknas"]})))
  (let [[[{:keys [message]}] {:keys [desc-sv]}] (sanitize-values
                                                 empty-string-message
                                                 {:desc-sv
                                                  ["" "Beskrivning saknas"]})]
    (is (= "Beskrivning saknas" desc-sv))
    (is (clojure.string/includes? message (empty-string-message "")))))

(deftest summarize-dcat-errors-test
  (let [[n msg] (summarize-dcat-errors [])]
    (is (= n 0))
    (is (cljstr/includes? msg "OK")))
  (let [[n msg] (summarize-dcat-errors [[{:mangled-name "mjao" :message "This is b@@@d"}]])]
    (is (= 1 n))
    (is (cljstr/includes? msg "mjao"))
    (is (cljstr/includes? msg "b@@@d")))
  (let [[n msg] (summarize-dcat-errors [[{:mangled-name "mjao" :message "This is b@@@d"}
                                      {:mangled-name "mjao" :message "999"}]])]
    (is (= n 2))))

(deftest compute-versions-to-download-test
  (is (= [19] (latest-version-seq-that-has-not-been-published-yet nil [17 19 18])))
  (is (= [18] (latest-version-seq-that-has-not-been-published-yet [19] [18 19])))
  (is (= [18] (latest-version-seq-that-has-not-been-published-yet [16 19] [17 18 19])))
  (is (= [17] (latest-version-seq-that-has-not-been-published-yet [16 18 19] [17 18 19])))  
  (is (= [19] (latest-version-seq-that-has-not-been-published-yet [0] [10 11 17 19 18])))
  (is (= [19] (latest-version-seq-that-has-not-been-published-yet [0] [10 11 17 19 18])))
  (is (= [19] (latest-version-seq-that-has-not-been-published-yet [0] [10 11 17 19 18])))
  (is (= [19] (latest-version-seq-that-has-not-been-published-yet [17 18 19] [17 18 19])))
  (is (= [19] (latest-version-seq-that-has-not-been-published-yet [11 17 18 0] [10 11 17 19 18])))
  (is (= [19] (latest-version-seq-that-has-not-been-published-yet [7 8 11 9 18 0] [10 11 17 19 18]))))

(deftest uri-test
  (is (not= "https://katt.com/mjao/a/b"
            (str (published-uri-for-local-file (URI. "https://katt.com/mjao")
                                               "/tmp"
                                               "/tmp/a/b/"))))
  (is (= "https://katt.com/mjao/a/b"
         (str (published-uri-for-local-file (URI. "https://katt.com/mjao/")
                                            "/tmp"
                                            "/tmp/a/b/")))))
