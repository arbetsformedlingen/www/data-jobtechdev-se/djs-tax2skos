(ns tax2skos.aws-interop-test
  (:require [tax2skos.aws-interop :refer :all]
            [clojure.test :refer :all])
  (:import [software.amazon.awssdk.auth.credentials AnonymousCredentialsProvider]
           [java.nio.file Files]
           [java.nio.file.attribute FileAttribute]))

(def parsed-config {:adhoc
                    {:type "s3",
                     :access_key_id "ADHOC_ID_GOES_HERE",
                     :secret_access_key "ADHOC_PASSWORD_GOES_HERE",
                     :region "eu-central-1"},
                    
                    :katt
                    {:type "s3",
                     :access_key_id "katt",
                     :secret_access_key "KATT_PASSWORD_GOES_HERE"}})

(deftest test-parse
  (is (= parsed-config (parse-config sample-config)))
  (let [r (.resolveCredentials (credentials-provider {:default {:aws_access_key_id "THE_ID" :aws_secret_access_key "THE_KEY"}}))]
    (is (= "THE_ID" (.accessKeyId r)))
    (is (= "THE_KEY" (.secretAccessKey r))))
  (let [r (.resolveCredentials (credentials-provider {}))
        i (.accessKeyId r)]
    (is (or (nil? i) (string? i))))
  (is (= "9" (version-from-path "/taxonomy/version/9/query/version//katt.json")))
  (is (= "mjao" (version-from-path "/taxonomy/version/mjao/query/katt.json")))
  (is (nil? (version-from-path "/taxonomy/versions/mjao/query/katt.json")))
  (is (nil? (list-s3-bucket-taxonomy-versions "s3://asdfsdsdfdjsjobtech" (AnonymousCredentialsProvider/create) "eu-central-1"))))

(deftest load-and-merge-configs-test
  (let [empty-attribs (make-array FileAttribute 0)
        creds-file (.toFile (Files/createTempFile "credentials" "x" empty-attribs))
        missing-creds (.toFile (Files/createTempFile "credentials" "x" empty-attribs))
        lower-priority-creds (.toFile (Files/createTempFile "credentials" "x" empty-attribs))
        config-file (.toFile (Files/createTempFile "config" "x" empty-attribs))
        _ (do (spit creds-file "[default]
username=august
password=t0rrfod3r")
              (spit lower-priority-creds "[default]
username=bossi
password=maskros")
              
              (spit config-file "[default]
region=afrika"))

        full-config (load-and-merge-configs [missing-creds creds-file lower-priority-creds config-file])]
    (is (= {:default {:region "afrika", :username "august", :password "t0rrfod3r"}}
           full-config))))

(deftest get-config-value-test
  (is (= "august" (get-config-value {:default {:username "august"}
                                     :adhoc {:username "bossi"}}
                                    :username)))
  
  (is (= "bossi" (get-config-value {:default {}
                                    :adhoc {:username "bossi"}}
                                   :username)))
  (is (nil? (get-config-value {:default {}
                               :adhoc {}}
                              :username))))

(deftest test-parse-path
  (let [path0 "taxonomy/yrkesområden-med-relationer-till-ssyk-nivå-fyra-och-yrkesbenämningar.json.dcat.json"
        {:keys [features filetype version path] :as parsed} (parse-path path0)]
    (is (= path0 path))
    (is (nil? version))
    (is (= #{:taxonomy} features))
    (is (= :dcat-json filetype))
    (is (dcat-file-to-remove? parsed))
    (is (dcat-file-to-remove? (assoc parsed :version "19")))
    (is (not (dcat-file-to-remove? (assoc parsed :version "latest"))))
    (is (not (dcat-file-to-remove? (assoc parsed :filetype :json))))))
