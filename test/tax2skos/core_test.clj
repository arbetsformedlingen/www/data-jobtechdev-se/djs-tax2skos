(ns tax2skos.core-test
  (:require [clojure.test :refer :all]
            [tax2skos.core :refer :all]))

(deftest a-test
  (is (= "xyz/" (common-prefix "xyz/abc" "xyz/123")))
  (is (= "xyz/" (trim-prefix (common-prefix "xyz/C0" "xyz/C1"))))
  (is (= "xyzAbc" (snake->camel-case "xyz-abc")))
  (is (= "123" (remove-prefix "xyz/" "xyz/123"))))

(deftest clean-uri-test
  (let [teststring (apply str ["  "  zero-width-no-break-space "abc   \n " zero-width-no-break-space
                               zero-width-no-break-space])]
    (is (not= "abc" teststring))
    (is (= "abc" (clean-uri teststring)))))

(deftest common-prefix-test
  (is (= (common-prefix "http://data.europa.eu/esco/skill/ab7260bb-457b-472f-84e2-2527c607b81b"
                        "﻿http://data.europa.eu/esco/skill/001d46db-035e-4b92-83a3-ed8771e0c123")
         ""))
  (is (= (common-prefix (clean-uri "http://data.europa.eu/esco/skill/ab7260bb-457b-472f-84e2-2527c607b81b")
                        (clean-uri "﻿http://data.europa.eu/esco/skill/001d46db-035e-4b92-83a3-ed8771e0c123"))
         "http://data.europa.eu/esco/skill/")))



(defn concept [id rel others]
  (conj {:id id
         :preferred_label (str "Label for " id)
         :type "occupation"
         :skos-uri (str "uri:" id)
         rel (into #{} (for [o others]
                         {:id o}))}))

(def cm0 {"a" (concept "a" :broader ["b" "c"])
          "b" (concept "b" :broader ["c"])
          "c" (concept "c" :broader [])})
(def cm0+ {"a" (concept "a" :broader ["b"])
           "b" (concept "b" :broader ["c"])
           "c" (dissoc (concept "c" :broader []) :broader)})

(def cm1 {"a" (concept "a" :broader [])
          "b" (concept "b" :narrower ["a"])})
(def cm1+ {"a" (concept "a" :broader ["b"])
           "b" (concept "b" :narrower ["a"])})

(deftest broader-tests
  (is (= cm0+ (-> cm0
                  remove-redundant-broader
                  cleanup-concept-map)))
  (is (= cm1+ (complete-with-reverse cm1 :narrower :broader))))

#_(deftest broad-match-test
  (is (= {:type "occupation-name",
          :broader #{{:id "c"}},
          :broad_match #{{:id "b"}}}
         (-> {"a" {:type "occupation-name" :broader #{{:id "b"} {:id "c"}}}
                                    "b" {:type "isco-level-4"}
                                    "c" {:type "ssyk-level-4"}}
             (broader-to-broad-match default-broad-match?)
             (get "a")))))
