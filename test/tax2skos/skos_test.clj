(ns tax2skos.skos-test
  (:require [clojure.test :refer :all]
            [clojure.spec.alpha :as spec]
            [tax2skos.skos :refer :all :as skos])
  (:import [java.net URI]))

(deftest spec-test
  (is (spec/valid?
       ::skos/data
       [[:prefix
         :jobtechType
         (URI. "http://data.jobtechdev.se/taxonomy/types/")]
        [:prefix
         :rdf
         (URI. "http://www.w3.org/1999/02/22-rdf-syntax-ns#")]
        [:prefix
         :rdfs
         (URI. "http://www.w3.org/2000/01/rdf-schema#")]
        [:prefix
         :owl
         (URI. "http://www.w3.org/2002/07/owl#")]
        [:prefix
         :skos
         (URI. "http://www.w3.org/2004/02/skos/core#")]
        [:base (URI. "http://example.org/ns/")]
        [:triplet :jobtechType/employment-duration :rdf/type :skos/Concept]
        [:triplet :jobtechType/sni-level-3 :rdf/type :skos/Concept]
        [:triplet :jobtechType/sni-level-4 :rdf/type :skos/Concept]
        [:triplet :jobtechType/country :rdf/type :skos/Concept]])))
