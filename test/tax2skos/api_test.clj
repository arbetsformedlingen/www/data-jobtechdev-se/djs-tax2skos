(ns tax2skos.api-test
  (:require [clojure.test :refer [deftest is]]
            [tax2skos.api :as api]))

(deftest map-from-edn-kv-pairs-test
  (is (= {:a 33, :b 44}
         (api/map-from-edn-kv-pairs [":a" "33" ":b" "44"]))))

(defn f0 [args]
  (+ (:x args) 100))

(defn f1 [args]
  (* (:y args) 10))

(deftest functions-cli
  (let [cli (api/functions-cli f0 f1)]
    (is (= 119 (cli "f0" ":x" "19")))
    (is (= 90 (cli "f1" ":y" "9")))))
