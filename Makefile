.PHONY: publish clean download no-op test clean-code dcat-processor all render fetch-versions


## DCAT processor code
dcat_jar_raw = build_code/dcat_processor/target/dcat-ap-processor-0.0.2-SNAPSHOT.jar.original
dcat_jar = build_code/dcat.jar
dcat_dir = build_code/dcat_processor
dcat_java_dir = $(dcat_dir)/src/main/java/se/ams/dcatprocessor
dcat_src = $(dcat_dir)/pom.xml $(dcat_java_dir)/Manager.java $(dcat_java_dir)/Application.java
dcat_sha = 7fa4ebb00f64ad5f1001900ecc41ecf04465df24
dcat_processor_zip_file = "$(dcat_sha).zip"
dcat_processor_url = "https://github.com/DIGGSweden/DCAT-AP-SE-Processor/archive/$(dcat_processor_zip_file)"

clj_deps = $(dcat_jar)


## Source code
src_core = src/tax2skos/core.clj
src_skos = src/tax2skos/skos.clj
src_download = src/tax2skos/dataset_downloader.clj
src_all = $(src_core) $(src_skos) $(src_download)

## Tasks
test: $(clj_deps)
	clj -X:test

deps: $(clj_deps)
	clj -A:task -P

clean:
	rm -rf build

clean-code:
	rm -rf build_code && rm -f $(app_jar)

fetch-versions: $(published_versions)

download: $(download_index)

no-op:
	clj -X:task no-op

render: $(derived_index)

## Targets
include Makefile_runtime.mk

rederive:
	rm $(derived_index) && make $(derived_index)

reset-build-from-backup:
	rm -rf build && cp -r build-bak2mini build

## Docker
docker-image:
	podman build -t tax2skos .

docker-run:
	podman run --memory=1280m tax2skos

docker-shell:
	podman run -it tax2skos /bin/bash

## OpenShift
oc-clean:
	oc delete is tax2skos && oc delete bc tax2skos && oc delete dc tax2skos

oc-new-app:
	oc new-app https://gitlab.com/arbetsformedlingen/taxonomy-dev/backend/tax2skos.git

oc-buildconfig:
	oc apply -f BuildConfig.yaml

oc-startbuild:
	oc start-build tax2skos-mjao

oc-job:
	oc apply -f Job.yaml

oc-delete-job:
	oc delete job tax2skos-job

oc-volume:
	oc set volume dc/tax2skos --name kattskit --type emptyDir --add

oc-delete-volume:
	oc set volume dc/tax2skos --name kattskit --remove

dcat-processor: $(dcat_jar)

$(dcat_src):
	mkdir -p build_code && cd build_code && rm -rf dcat_processor && wget $(dcat_processor_url) && unzip $(dcat_processor_zip_file) && rm $(dcat_processor_zip_file) && mv * dcat_processor && python3 ../edit_dcat.py dcat_processor

# This target simply builds the source code
$(dcat_jar): $(dcat_src)
	(cd $(dcat_dir) && mvn -DskipTests clean package spring-boot:repackage) && cp $(dcat_jar_raw) $(dcat_jar)

$(app_jar): $(clj_deps)
	clj -T:build uber

uberjar: $(app_jar)
