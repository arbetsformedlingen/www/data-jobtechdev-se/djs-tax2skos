app_jar = target/app.jar
run_clj_task = java -Xmx512m -jar $(app_jar)
result_root = $(build_root)/result

## This file contains a collection of all versions that have been published.
published_versions = $(build_root)/published_versions.json

## This file is produced at the end of downloading the taxonomy
## and lists all the files that were downloaded.
download_index = $(build_root)/download_index.json

## This file is produced at the end of deriving other exports
## of the taxonomy. It contains information about the exports.
derived_index = $(build_root)/derived_index.json

## This variables lists the targets needed to export *all* formats.
result_zip = build/taxonomy.zip
result_files = $(derived_index)

build_root = build
djs_root = $(build_root)/jobtechdev_data
jobtechdev_data = $(djs_root)/html/taxonomy
debug_root = $(build_root)/debug
dcat_debug_root = $(debug_root)/dcat




$(published_versions): $(app_jar)
	$(run_clj_task) fetch-published-versions :bucket-files '["/secrets/BUCKET" "DEFAULT_BUCKET"]' :config-files '["/secrets/credentials" "/secrets/config" "/secrets/rclone-secrets.conf" "rclone-secrets.conf"]' :output-file '"$(published_versions)"'

$(download_index): $(app_jar) $(published_versions)
	mkdir -p "$(build_root)" && $(run_clj_task) download-all-datasets :spec-filename '"https://gitlab.com/arbetsformedlingen/www/data-jobtechdev-se/djs-tax2skos/-/raw/main/taxonomy-dataset-3.csv"' :output-root '"$(result_root)"' :index-filename '"$(download_index)"' :published-versions-file '"$(published_versions)"'

$(derived_index): $(app_jar) $(download_index)
	$(run_clj_task) derive-files :index-filename '"$(download_index)"' :derived-index-filename '"$(derived_index)"' :debug-output-root '"build/debug"'  :output-root '"$(result_root)"' :dcat-template-file '"https://gitlab.com/arbetsformedlingen/www/data-jobtechdev-se/djs-tax2skos/-/raw/main/resources/dcat_template.json"' :dcat-catalog-file '"https://gitlab.com/arbetsformedlingen/www/data-jobtechdev-se/djs-tax2skos/-/raw/main/resources/catalog.json"'

publish: $(result_files)
	mkdir -p $(jobtechdev_data) && cp -r $(result_root)/* $(jobtechdev_data)/. && bash publish2djs.sh $(djs_root)

$(result_zip): $(result_files)
	cd $(result_root) && zip -r ../../$(result_zip) *

all: $(result_zip)
