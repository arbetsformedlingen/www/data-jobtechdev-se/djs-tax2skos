# djs-tax2skos

This program downloads the entire JobTech Taxonomy and exports it to [SKOS](https://en.wikipedia.org/wiki/Simple_Knowledge_Organization_System).

## How it works

djs-tax2skos is deployed on the JobTech infrastructure as a cron job, see the repository [djs-tax2skos-infra](https://gitlab.com/arbetsformedlingen/www/data-jobtechdev-se/djs-tax2skos-infra) for how this is configured. The file named `kustomization.yaml` mentioning `newTag: 519479ba` points out which commit to use.

Every hour (or whatever frequency is configured), this cron job will call `make publish` (see [`Dockerfile`](Dockerfile).

The `make publish` command results in the following steps being performed:

1. Query the [jobtech-taxonomy-api][100] for all versions of the taxonomy. This step can be performed by calling `make fetch-versions`. The result is the file `build/published_versions.json` being produced.
2. For every version, run a series of GraphQL queries specified in [`taxonomy-dataset-3.csv`](taxonomy-dataset-3.csv) against the jobtech-taxonomy-api. This step can be performed by calling `make download`. The result is a set of files saved to the `build` directory.
3. From all the downloaded data in the `build` directory, produce more files in different formats. This step can be performed by calling `make render`. It will output more files to the `build` directory.
4. Upload the rendered files to an S3 bucket. This step is performed by `make publish`. Internally it will call the bash script `bash publish2djs.sh`. That bash script accesses the S3 bucket using secrets in OpenShift. Inside the container, those secrets are found in a file put there by OpenShift.

To experiment locally there are various targets defined in [`Makefile`](Makefile). You can run the commands `make fetch-versions`, `make download`, `make render` and `make publish` to perform the above steps.

## Code overview

The code to perform the above steps is found under [`src/tax2skos`](src/tax2skos). Here is a brief explanation of what the files do:

| File                     | Explanation                                                                                     |
|--------------------------|-------------------------------------------------------------------------------------------------|
| `api.clj`                | Contains the **tasks** called in the Makefile, e.g. `clj -X:task fetch-published-versions`      |
| `aws_interop.clj`        | Used to interact with AWS S3 buckets, especially to see what files have already been published. |
| `core.clj`               | Transforms the downloaded data to SKOS.                                                         |
| `dataset_downloader.clj` | Runs the GraphQL queries against the API to download data and also produces DCAT files.         |
| `dataset_spec.clj`       | Specification of the [`taxonomy-dataset-3.csv`](taxonomy-dataset-3.csv) input file.             |
| `dcat.clj`               | Code to produce DCAT files.                                                                     |
| `graphql.clj`            | Manipulate GraphQL queries to provide paging and version arguments.                             |
| `graphql_request.clj`    | Run GraphQL request against the API.                                                            |
| `skos.clj`               | Functions to render SKOS.                                                                       |
| `utils.clj`              | Various general purpose functions.                                                              |
| `xml.clj`                | Functions to work with XML.                                                                     |

## Where files are published

In production, the files are published at https://data.jobtechdev.se/. The published taxonomy data is found at in the subdirectory `taxonomy/`, with url `https://data.jobtechdev.se/taxonomy`. Note that this website is produced by by a cron job in another project that scans the AWS S3 buckets. Therefore, there can be a delay before the files produced by `djs-tax2skos` become visible after they have been published. See the route definitions in [`djs-frontend-infra`](https://gitlab.com/arbetsformedlingen/www/data-jobtechdev-se/djs-frontend-infra/-/tree/main/kustomize/overlays?ref_type=heads) for the actual route definitions.

Under **Secrets** in OpenShift for this project `djs-tax2skos` , there is a secret that points out which S3 bucket `djs-tax2skos` uses.

## Usage

First, make sure the [required software](#requirements) is installed.

Second, type `make all` to produce the file `build/taxonomy_skos.zip` that contains the JobTech taxonomy in SKOS format (Turtle and XML).

### Docker

There is a [Dockerfile](Dockerfile) that defines an image for exporting the taxonomy to SKOS for publishing on data.jobtechdev.se. To build all the files using Docker, do

```
make docker-image
make docker-run
docker cp 797faf2c68b0:/tax2skos/build/jobtechdev_data /tmp/.
```
where `797faf2c68b0` is the name of the container (you need to edit it).

## A note about logging

This project uses [clojure.tools.logging](https://clojure.github.io/tools.logging/) to perform logging. The actual logging is performed by a logging backend that is detected automatically by that library.

It seems like [Logback](https://logback.qos.ch/) is the library currently used. You can edit the file [resources/logback.xml](resources/logback.xml) to configure it.


## Requirements

* [Make](https://www.gnu.org/software/make/)
* [Clojure CLI](https://clojure.org/guides/getting_started#_installation_on_linux)
* [Skosify](https://github.com/NatLibFi/Skosify), installed using `pip3 install skosify`.

## Links about SKOS

Here is more information about SKOS and resources related to SKOS. There are other libraries for working with SKOS listed below.

### Specific Questions

* [About multiple broader](https://www.athenaeurope.eu/getFile.php?id=684): *"One concept can have more than one broader concept or more than one narrower concept."*


### SKOS Resources

* [SKOS Primer](https://www.w3.org/TR/skos-primer/#secskosowl), [outdated version](https://www.w3.org/TR/2009/NOTE-skos-primer-20090818/)
* [SKOS Reference](https://www.w3.org/TR/skos-reference/), [outdated version](https://www.w3.org/2009/08/skos-reference/skos.html)
* [SKOS API, Git repository of Java code](https://github.com/simonjupp/java-skos-api)
* [ESCO classes](http://akswnc7.informatik.uni-leipzig.de/dstreitmatter/archivo/data.europa.eu/esco--model/2020.10.13-180035/esco--model_type=generatedDocu.html#classes)
* [About classes in SKOS/OWL](https://www.w3.org/TR/skos-primer/#secskosowl)
* [Example of link to type](www.w3.org/2002/07/owl#NamedIndividual)
* [OWL API](https://github.com/owlcs/owlapi/tree/version5/api/src/main/java/org/semanticweb/owlapi)
* [How ESCO is modeled](https://www.researchgate.net/profile/Adham-Kahlawi/publication/340402371_An_Ontology_Driven_ESCO_LOD_Quality_Enhancement/links/5ec506ada6fdcc90d685f89c/An-Ontology-Driven-ESCO-LOD-Quality-Enhancement.pdf). Information about `rdf:type` used for skills, occupations, etc.
* [ESCO RDF model](https://ec.europa.eu/esco/lod/static/model.html)
* [RDF prefices](https://github.com/linkeddata/ontology-archiver/blob/master/all.ttl)
* [Skosify](https://github.com/NatLibFi/Skosify)

### RDF4J

* [RDF4J](https://rdf4j.org/).
* [Javadoc](https://rdf4j.org/javadoc/latest/)
* [SKOS Javadoc](https://rdf4j.org/javadoc/3.1.0/index.html?org/eclipse/rdf4j/model/vocabulary/SKOS.html)
* [Info about the API](https://rdf4j.org/documentation/programming/model/)

## License

Copyright © 2022 Arbetsförmedlingen JobTech

This program and the accompanying materials are made available under the
terms of the Eclipse Public License 2.0 which is available at
http://www.eclipse.org/legal/epl-2.0.

This Source Code may also be made available under the following Secondary
Licenses when the conditions for such availability set forth in the Eclipse
Public License, v. 2.0 are satisfied: GNU General Public License as published by
the Free Software Foundation, either version 2 of the License, or (at your
option) any later version, with the GNU Classpath Exception which is available
at https://www.gnu.org/software/classpath/license.html.

[100]: https://gitlab.com/arbetsformedlingen/taxonomy-dev/backend/jobtech-taxonomy-api
