FROM clojure:temurin-17-tools-deps-1.11.3.1456-jammy AS builder

WORKDIR /build
ENV HOME /build
COPY . .

RUN apt-get update && apt-get install -y \
    zip wget maven &&\
    useradd -m jobtechuser &&\
    make deps && make uberjar

FROM clojure:temurin-17-tools-deps-1.11.3.1456-jammy AS runtime

RUN apt-get update && apt-get install -y \
    zip rclone &&\
    useradd -m jobtechuser

WORKDIR /app
COPY --from=builder /build/target/app.jar target/app.jar
COPY --from=builder /build/Makefile_runtime.mk Makefile
COPY --from=builder /build/publish2djs.sh publish2djs.sh
RUN chown -R jobtechuser:jobtechuser /app
USER jobtechuser
CMD ["make", "publish"]