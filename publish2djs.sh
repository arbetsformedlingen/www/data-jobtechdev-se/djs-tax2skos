#!/usr/bin/env bash
echo "Publish ${1} to data.jobtechdev.se..."
cd "${1}"
. /secrets/rclone-source.conf
rclone -vv  --transfers=128 --checkers=128 --config /secrets/rclone-secrets.conf copyto html ${SOURCE}
