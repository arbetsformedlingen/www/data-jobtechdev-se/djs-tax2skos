from pathlib import Path
import sys

root = Path(sys.argv[1])

def edit_file(rel_path, *replacements):
    path = root.joinpath(rel_path)
    print(f"-- EDIT FILE '{str(path)}'")
    s = path.read_text()
    for (old_subs, new_subs) in replacements:
        s_next = s.replace(old_subs, new_subs)
        if s_next == s:
            raise RuntimeError(f"Failed to replace '{old_subs}' by '{new_subs}'")
        s = s_next
    path.write_text(s)

edit_file("src/main/java/se/ams/dcatprocessor/Manager.java",
          ('printToFile(result, "dcat.rdf");',
           'printToFile(result, "build/debug/dcat.rdf");'))
edit_file("src/main/java/se/ams/dcatprocessor/parser/ApiDefinitionParser.java",
          ('"output.json"',
           '"build/debug/output.json"'))
