(ns tax2skos.graphql
  (:require [clojure.tools.logging :as log])
  (:import [graphql.parser Parser]
           [com.google.common.collect ImmutableList]
           [java.math BigInteger BigDecimal]
           [java.util.function Consumer]
           [graphql.language AstPrinter
            OperationDefinition$Operation Argument FloatValue IntValue StringValue]
           [graphql.parser
            InvalidSyntaxException]))

(defn- wrap-value [v]
  (cond
    (int? v) (IntValue. (BigInteger/valueOf v))
    (string? v) (StringValue. v)
    (or (float? v) (double? v)) (FloatValue. (BigDecimal. v))))

(defn immutable-list [coll]
  (ImmutableList/copyOf (if (nil? coll) [] coll)))

(defmacro node-assoc [dst method new-value]
  `(.transform
    ~dst
    (proxy [Consumer] []
      (accept [builder#]
        (~method builder# ~new-value)))))

(defmacro with-node-prop [dst method [bind-sym get-method] & body]
  `(node-assoc
    ~dst ~method
    (let [~bind-sym (~get-method ~dst)]
      ~@body)))

(defn apply-parameters-to-selection [selection query-parameter-map]
  (let [k (.getName selection)]
    (if-let [params (get query-parameter-map k)]
      (with-node-prop
        selection
        .arguments
        [arguments .getArguments]
        (let [arg-map (into {} (map (fn [arg] [(.getName arg) arg])) arguments)]
          (immutable-list
           (vals (into arg-map (for [[k v] params
                                     :let [wrapped (wrap-value v)]
                                     :when (some? wrapped)]
                                 [k (Argument. k wrapped)]))))))
      selection)))

(defn provide-query-selection-arguments [^graphql.language.Document doc query-parameter-map]
  (with-node-prop
    doc
    .definitions
    [definitions .getDefinitions]
    (for [def definitions]
      (if (= OperationDefinition$Operation/QUERY (.getOperation def))
        (with-node-prop
          def
          .selectionSet
          [selection-set .getSelectionSet]
          (with-node-prop
            selection-set
            .selections
            [selections .getSelections]
            (for [selection selections]
              (apply-parameters-to-selection selection query-parameter-map))))
        def))))

(defn flatten-doc-selections [^graphql.language.Document doc]
  (for [def (.getDefinitions doc)
        :let [sel-set (.getSelectionSet def)]
        selection (.getSelections sel-set)]
    {:definition def
     :selection selection}))

(defn parse [graphql-expr]
  (try
    (Parser/parse graphql-expr)
    (catch InvalidSyntaxException e
      (log/error "Failed to parse GraphQL at" (.getLocation e))
      (log/error graphql-expr)
      (throw e))))

(defn parsable? [expr]
  (and (string? expr)
       (try
         (Parser/parse expr)
         true
         (catch InvalidSyntaxException _
           false))))

(defn render-compact [^graphql.language.Document graphql-doc]
  (AstPrinter/printAst graphql-doc))
