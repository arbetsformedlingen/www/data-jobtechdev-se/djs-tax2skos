(ns tax2skos.xml
  (:require [clojure.data.xml :as xml]
            [clojure.java.io :as io]
            [clojure.string :as cljstr]
            [clojure.data.xml.node :as node])
  (:import [clojure.data.xml.node Element]))

(defn element? [x]
  (instance? Element x))

(defn xml-sort-feature [node]
  (if (map? node)
    (let [c (:content node)]
      (into [(:tag node)]
            (comp cat cat)
            [(sort-by key (:attrs node))
             [(if (and (= 1 (count c))
                       (string? (first c)))
                [(first c)]
                [])]]))
    node))

(defn blank-string? [x]
  (and (string? x)
       (cljstr/blank? x)))

(defn remove-whitespace [content]
  (if (some map? content)
    (remove blank-string? content)
    content))

(declare normalize-xml-recursively)

(defn normalize-content [content]
  (->> content
       remove-whitespace
       (map normalize-xml-recursively)
       (sort-by xml-sort-feature)))

(defn normalize-xml-recursively [data]
  (if (element? data)
    (assoc data :content (normalize-content (:content data)))
    data))

(defn write-xml [file data]
  (with-open [f (io/writer file)]
    (xml/emit data f)))

(defn read-xml [file]
  (-> file
      slurp
      xml/parse-str))

(defn normalize-xml-file-in-place [file]
  (->> (read-xml file)
       normalize-xml-recursively
       (write-xml file)))

(comment
  (do

    (def input-filename0 "/home/jonas/Downloads/language-level.v1.xml")
    (def input-filename1 "/home/jonas/Downloads/language-level.v2.xml")

    
    (def data0 (xml/parse-str (slurp input-filename0)))
    (def data1 (xml/parse-str (slurp input-filename1)))
    
    (def testfile "/tmp/mjao.xml")
    (write-xml testfile data0)

    ))
