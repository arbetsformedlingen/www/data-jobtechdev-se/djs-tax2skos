(ns tax2skos.core
  (:require [cheshire.core :as cheshire]
            [clojure.spec.alpha :as spec]
            [clojure.string :as cljstr]
            [tax2skos.skos :as skos]
            [tax2skos.dataset-spec :refer [remove-newlines]]
            [clojure.java.io :as io]
            [clojure.java.shell :as shell]
            [tax2skos.xml :refer [normalize-xml-file-in-place]]
            [tax2skos.utils :as utils]
            [clojure.tools.logging :as logging])
  (:import [java.net URI]))

;;;------- How different properties of a concept are handled -------

(defn concept? [c]
  (and (map? c)
       (every?
        (partial contains? c)
        [:id :preferred_label :type :skos-uri])))

(defn validate-concept-map [concept-map]
  (doseq [[k concept] concept-map]
    (when-not (concept? concept)
      (throw (ex-info "Invalid concept at key" {:key k :concept concept}))))
  concept-map)

(defn single-value
  ([k f]
   (fn [x] (if-let [v (f x)] [v] [])))
  ([k]
   (single-value k #(get % k))))

(defn multiple-values [k]
  (fn [x] (get x k)))

(defn multiple-ids [k]
  (fn [x] (map :id (get x k))))

(defn constant-value [x]
  (fn [_] [x]))



(defn remove-prefix [prefix x]
  (if (cljstr/starts-with? x prefix)
    (subs x (count prefix))))

(defn non-empty-string? [x]
  (and (string? x)
       (< 0 (count x))))

;; This is the key used to refer to a concept in the turtle output.
(defn concept-ref [scheme-map c]
  (let [type-info (get scheme-map (:scheme c))
        prefix-key (:key type-info)
        prefix-uri (:uri type-info)
        uri (:skos-uri c)
        _ (assert (non-empty-string? uri))
        rel (remove-prefix prefix-uri uri)]
    (assert rel)
    (keyword (:key type-info) rel)))

(defn prop-value [k]
  [k (fn [context x]
       [[k x]])])

(defn prop-langstr [k langkey]
  {:pre [(= "lang" (namespace langkey))]}
  [k (fn [context x]
       (assert (string? x))
       [[k [x langkey]]])])

(defn prop-ref [k]
  [k (fn [context x]
       (or (if-let [dst (get-in context [:concept-map x])]
             (if (empty? (:errors dst))
               [[k (concept-ref (:scheme-map context) dst)]])
             (do (println "Missing concept" k x)
                 []))))])

(defn common-prefix [^String a ^String b]
  (let [n (min (count a) (count b))]
    (loop [i 0]
      (if (or (= i n) (not= (.charAt a i)
                            (.charAt b i)))
        (subs a 0 i)
        (recur (inc i))))))

(defn prop-info [getter [prop-key maker]]
  {:getter getter
   :key prop-key
   :maker maker})

(defn top-concept? [c]
  (and (:native c) (empty? (:broader c))))

(defn get-top-concept-of [c]
  (if (top-concept? c)
    (:scheme c)))

;; Properties (2. SKOS Namespace and Vocabulary)
(def properties [(prop-info (constant-value :skos/Concept)
                            (prop-value :rdf/type))
                 (prop-info (single-value :scheme) (prop-value :skos/inScheme))
                 (prop-info (single-value :top-concept-of get-top-concept-of)
                            (prop-value :skos/topConceptOf))
                 (prop-info (single-value :preferred_label)
                            (prop-langstr :skos/prefLabel :lang/sv))
                 (prop-info (single-value :definition)
                            (prop-langstr :skos/definition :lang/sv))
                 (prop-info (multiple-values :alternative_labels)
                            (prop-langstr :skos/altLabel :lang/sv))
                 (prop-info (multiple-ids :broader) (prop-ref :skos/broader))
                 (prop-info (multiple-ids :related) (prop-ref :skos/related))
                 (prop-info (multiple-ids :narrower) (prop-ref :skos/narrower))
                 (prop-info (multiple-ids :exact_match) (prop-ref :skos/exactMatch))
                 (prop-info (multiple-ids :close_match) (prop-ref :skos/closeMatch))
                 (prop-info (multiple-ids :broad_match) (prop-ref :skos/broadMatch))
                 (prop-info (multiple-ids :narrow_match) (prop-ref :skos/narrowMatch))])

(def base-uri "http://data.jobtechdev.se/taxonomy/")

(def foreign-concept-types #{"esco-skill" "esco-occupation"})







;;;------- Broader simplification -------

(def relation-keys [:broader :related :narrower :exact_match :close_match :broad_match :narrow_match])

(defn get-missing-referred-concept-ids [concept-map]
  (into #{} (for [[k concept] concept-map
                  relk relation-keys
                  {:keys [id]} (get concept relk) :when (not (concept-map id))]
              id)))

(defn remove-missing-referred-concepts [concept-map missing-ids]
  (into {} (for [[k concept] concept-map]
             [k (reduce (fn [concept relk]
                          (update concept relk (partial into #{} (remove (comp missing-ids :id)))))
                        concept
                        relation-keys)])))

(defn follow-broader [concept-map cid current-id depth]
  (if (contains? concept-map current-id)
    (let [concept-map (update-in concept-map [current-id ::broader-path-lengths cid] (fn [l] (max (or l 0) depth)))
          broader-ids (map :id (get-in concept-map [current-id :broader]))]
      (reduce (fn [dst bid]
                (follow-broader dst cid bid (inc depth)))
              concept-map
              broader-ids))
    (do (println (format "Warning: Non-existing concept %s referred to by %s" current-id cid))
        concept-map)))

(defn concept-str [concept]
  (format "'%s (%s)'"
          (:preferred_label concept)
          (:type concept)))

(defn add-nonredundant-broader-relations [concept-map concept]
  (reduce (fn [concept-map [cid plen]]
            (if (= plen 1)
              (update-in concept-map [cid :broader] #(conj (or % #{}) {:id (:id concept)}))
              concept-map))
          concept-map
          (::broader-path-lengths concept)))

(defn remove-redundant-broader [concept-map]
  {:post [(validate-concept-map %)]}
  ;; Make a new concept-map *without* broader but with a map to all children
  ;; who refer to this concept as being broader
  (let [concept-map (into {}
                          (for [[k v] (reduce (fn [dst cid]
                                                (follow-broader dst cid cid 0))
                                              concept-map
                                              (keys concept-map))]
                            [k (dissoc v :broader)]))]

    ;; For every concept, rebuild broader relations to that concept
    (reduce add-nonredundant-broader-relations
            concept-map
            (vals concept-map))))

(defn cleanup-concept-map [concept-map]
  {:post [(validate-concept-map %)]}
  (into {} (for [[k v] concept-map]
             [k (dissoc v ::broader-path-lengths)])))

(defn complete-with-reverse [concept-map k0 k1]
  {:post [(validate-concept-map %)]}
  (reduce (fn [concept-map [src dst]]
            (if (contains? concept-map dst)
              (update-in concept-map [dst k1] #(conj (set %) {:id src}))
              concept-map))
          concept-map
          (for [concept (vals concept-map)
                {:keys [id]} (k0 concept)]
            [(:id concept) id])))

(defn reset-keys [concept-map & ks]
  {:post [(validate-concept-map %)]}
  (into {} (for [[k v] concept-map]
             [k (apply dissoc v ks)])))

;(def broad-match-dst-types #{"isco-level-4"})
;(def broad-match-src-dst-types #{["ssyk-level-4" "occupation-field"]})

(defn key-not= [k0 k1]
  {:pre [(keyword? k0)
         (keyword? k1)]}
  (not= k0 k1))

(defn broader-to-broad-match [concept-map]
  {:post [(validate-concept-map %)]}
  (into {} (for [[k concept] concept-map]
             (let [broader (:broader concept)
                   f #(not= (:scheme concept)
                            (get-in concept-map [(:id %) :scheme]))]
               [k (-> concept
                      (assoc :broader (set (remove f broader)))
                      (update :broad_match #(into (set %) (filter f) broader)))]))))

(defn check-for-multiple-broader [concept-map]
  {:post [(validate-concept-map %)]}
  (doseq [[k v] concept-map]
    (when (< 1 (count (:broader v)))
      (do (println "Multiple broader concepts at" (concept-str v))
          (doseq [k (:broader v)]
            (println "  * " (concept-str (get concept-map (:id k))))))))
  concept-map)

(defn correct-broader-issues [concept-map]
  (-> concept-map
      (complete-with-reverse :narrower :broader)
      remove-redundant-broader
      broader-to-broad-match
      check-for-multiple-broader
      (reset-keys :narrower :narrow_match)
      (complete-with-reverse :broader :narrower)
      (complete-with-reverse :broad_match :narrow_match)
      cleanup-concept-map
      ))

(def zero-width-no-break-space (char 65279))

(defn clean-uri [uri]
  (if uri 
    (-> uri
        (cljstr/escape {zero-width-no-break-space ""})
        cljstr/trim)))

(defn preprocess-concept [concept extra-fn]
  (let [foreign-uri (clean-uri (or (:esco_uri concept)))
        has-foreign-uri? (some? foreign-uri)
        ft (foreign-concept-types (:type concept))
        native (and (not has-foreign-uri?) (not ft))
        foreign has-foreign-uri?
        skos-uri (or foreign-uri (clean-uri (:uri concept)))
        errors (into []
                     cat
                     [(if (and ft (not has-foreign-uri?))
                        ["Foreign concept type but missing URI"]
                        [])])
        _ (assert (or (not foreign) (string? foreign-uri)))
        concept (-> concept
                    (update :alternative_labels (partial remove #{(:preferred_label concept)}))
                    (merge {:skos-uri skos-uri
                            :native native
                            :foreign foreign
                            :errors errors}))
        concept (merge concept (extra-fn concept))]
    concept))

;; SKOS reference 1.7.3
(def standard-decls
  [[:prefix :rdf (URI. "http://www.w3.org/1999/02/22-rdf-syntax-ns#")]
   [:prefix :rdfs (URI. "http://www.w3.org/2000/01/rdf-schema#")]
   [:prefix :owl (URI. "http://www.w3.org/2002/07/owl#")]
   [:prefix :skos (URI. "http://www.w3.org/2004/02/skos/core#")]
   [:prefix :dcterms (URI. "http://purl.org/dc/terms/")]
   [:base (URI. "http://example.org/ns/")]
   ])

(defn trim-prefix [^String prefix]
  (loop [i (count prefix)]
    (let [j (dec i)]
      (if (or (zero? i)
              (#{\/ \#} (.charAt prefix j)))
        (subs prefix 0 i)
        (recur j)))))

(defn snake->camel-case [x]
  {:pre [(string? x)]}
  (apply str (for [i (range (count x))
                   :let [c (nth x i)]
                   :when (not= \- c)]
               (if (= \- (get x (dec i)))
                 (Character/toUpperCase c)
                 c))))

(defn build-id-map [items]
  (into {} (map (juxt :id identity)) items))

(def jobtech-type-prefix "jobtechType")
(def jobtech-scheme-prefix "jobtechScheme")

(defn starting-with-fn [pref]
  #(cljstr/starts-with? % pref))

(def ssyk-level? (starting-with-fn "ssyk-level-"))

(defn occupation? [type]
  (or (ssyk-level? type) (#{"occupation-name"} type)))
(def sun-education-field? (starting-with-fn "sun-education-field"))
(def sun-education-level? (starting-with-fn "sun-education-level"))
(def geographic? #{"continent" "country" "region" "municipality"})
(def skill? #{"skill" "skill-headline"})
(def employment? #{"employment-variety" "employment-type" "self-employment-type"})
(def sni-level? (starting-with-fn "sni-level"))

(defn scheme-key [s]
  (if (qualified-keyword? s)
    s
    (keyword jobtech-scheme-prefix s)))

(defn composite-scheme [pred-fn key title]
  {:pre [(string? key)
         (string? title)]}
  {:pred-fn pred-fn
   :key (keyword jobtech-scheme-prefix key)
   :title title})

(def composite-schemes [(composite-scheme occupation? "occupation" "Yrke")
                        (composite-scheme sun-education-field? "sun-education-field" "SUN utbildningsområde")
                        (composite-scheme sun-education-level? "sun-education-level" "SUN utbildningsnivå")
                        (composite-scheme geographic? "geographic" "Geografi")
                        (composite-scheme skill? "skill" "Färdighet")
                        (composite-scheme employment? "employment" "Anställning")
                        (composite-scheme sni-level? "sni-level" "SNI-nivå")])

(defn composite-scheme-from-type [type]
  (first (filter #((:pred-fn %) type) composite-schemes)))

(defn scheme-from-type [type]
  {:pre [(string? type)]}
  (or (composite-scheme-from-type type)
      {:key (keyword jobtech-scheme-prefix type)}))

(defn scheme-key-from-type [type]
  (:key (scheme-from-type type)))

(defn concept-skos-data
  "Provides extra data needed for SKOS export for a concept"
  [concept]
  {:scheme (scheme-key-from-type (:type concept))
   :memberships #{}})

(defn find-disjoint-uri-pair [uris]
  (first (for [a uris
               b uris :when (empty? (common-prefix a b))]
           [a b])))

(defn build-scheme-prefix-map [all-concepts]
  (let [uri-to-scheme (into {}
                            (comp (filter (comp empty? :errors))
                                  (map (juxt :skos-uri :scheme)))
                            all-concepts)
        uris (keys uri-to-scheme)
        scheme-prefix-map (into {}
                                (for [[k scheme-uris] (group-by uri-to-scheme uris)]
                                  (do (assert (< 0 (count scheme-uris)))
                                      (let [prefix-uri (trim-prefix (reduce common-prefix scheme-uris))]
                                        (when (empty? prefix-uri)
                                          (throw (ex-info (str "Invalid prefix uri for scheme") {:scheme k
                                                                                                 :prefix-uri prefix-uri
                                                                                                 :disjoint-uri-pair
                                                                                                 (find-disjoint-uri-pair scheme-uris)})))
                                        [k {:uri prefix-uri
                                            :key (snake->camel-case (name k))}]))))]
    scheme-prefix-map)) 

(defn preprocess-concepts [concepts extra-fn]
  (let [all-concepts (for [c concepts]
                       (preprocess-concept c extra-fn))
        problematic-concepts (filter (comp seq :errors) all-concepts)
        concept-map (build-id-map all-concepts)
        missing-ids (get-missing-referred-concept-ids concept-map)
        concept-map (remove-missing-referred-concepts concept-map missing-ids)]

    (println "Number of concepts:" (count concept-map))
    (when (< 0 (count missing-ids))
      (println (format "There are %d referred concepts that are missing" (count missing-ids))))
    
    (doseq [p problematic-concepts
            err (:errors p)]
      (println (format "Problematic concept %s (%s): %s"
                       (:id p)
                       (:type p)
                       err)))
    
    (correct-broader-issues concept-map)))

(defn produce-skos-data [concept-map concepts-to-export collections scheme-map]
  {:pre [(map? concept-map)
         (sequential? concepts-to-export)
         (map? scheme-map)]}
  (let [collections (for [collection collections
                          :let [collection-concepts (filter (:concept-pred-fn collection) concepts-to-export)]
                          :when (seq collection-concepts)]
                      (assoc collection :concepts collection-concepts))
        context {:concept-map concept-map
                 :scheme-map scheme-map}
        scheme-groups (into {} (for [[scheme-key scheme-concepts] (group-by :scheme concepts-to-export)]
                                 [scheme-key (into #{} (map :type) scheme-concepts)]))
        result (into []
                     (comp cat cat)
                     [

                      (for [[_ {:keys [uri key]}] scheme-map]
                        ;; This is the prefix used with concepts belonging to this scheme
                        [[:prefix (keyword key) (URI. uri)]])
               
                      ;; Schemes
                      (for [[scheme-key {:keys [label]}] scheme-map
                            :let [types-in-scheme (scheme-groups scheme-key)]
                            :when (seq types-in-scheme)]
                        (let [description (str "Terminologi för begrepp av typerna "
                                               (cljstr/join ", " (for [t (sort types-in-scheme)]
                                                                   (format "'%s'" t))))]
                          (assert label)
                          [
                           ;; These are scheme-details
                           [:triplet scheme-key :rdf/type :skos/ConceptScheme]
                           [:triplet scheme-key :dcterms/title [label :lang/sv]]
                           [:triplet scheme-key :rdfs/label [label :lang/sv]]
                           [:triplet scheme-key :dcterms/description [description :lang/sv]]
                           [:triplet scheme-key :dcterms/publisher "https://jobtechdev.se/en"]]))

                      (for [{:keys [key label concepts description]} collections]
                        (into [[:triplet key :rdf/type :skos/Collection]
                               [:triplet key :dcterms/title [label :lang/sv]]
                               [:triplet key :rdfs/label [label :lang/sv]]
                               [:triplet key :dcterms/description [description :lang/sv]]]
                              (for [concept concepts]
                                [:triplet key :skos/member (concept-ref scheme-map concept)])))

                      [standard-decls
                
                       ;; Prefix used by the schemes
                       [[:prefix (keyword jobtech-type-prefix) (URI. (str base-uri "type/"))]
                        [:prefix (keyword jobtech-scheme-prefix) (URI. (str base-uri "scheme/"))]]

                       ;; Concepts
                       (for [concept concepts-to-export
                             :when (:native concept)
                             {:keys [getter maker]} properties
                             x (getter concept)
                             prop (maker context x)]
                         (into [:triplet (concept-ref scheme-map concept)] prop))]])]
    result))

(defn make-title [s]
  (str "JobTech Taxonomy - " s))

(defn scheme-map-from-types [types prefix-map]
  (let [m (into {} (for [type types]
                     (let [d (scheme-from-type (:id type))]
                       [(:key d) {:label (make-title
                                          (or (:title d)
                                              (:label_sv type)))}])))
        result (into {}
                     (for [[k pref-data] prefix-map]
                       [k (merge pref-data (get m k))]))]
    (assert (= (keys result) (keys prefix-map)))
    result))


(defn collection-from-type [{:keys [id label_sv]}]
  {:key (keyword jobtech-type-prefix id)
   :label (make-title label_sv)
   :description (format "Begrepp av typen '%s' (kod '%s')" label_sv id)
   :concept-pred-fn #(= id (:type %))})

(defn write-skos-files [output-dir concepts types]
  {:pre [(sequential? concepts)
         (sequential? types)]}
  (let [concept-map (preprocess-concepts concepts concept-skos-data)
        all-concepts (vals concept-map)
        concepts-per-scheme (group-by :scheme (filter :native all-concepts))
        scheme-prefix-map (build-scheme-prefix-map all-concepts)
        scheme-map (scheme-map-from-types types scheme-prefix-map)
        concept-types-per-scheme (into {} (map (fn [[k v]] [k (into #{} (map :type) v)])) concepts-per-scheme)
        raw-dir (io/file output-dir "raw")
        ttl-dir (io/file raw-dir "ttl")
        scheme-file-map (into {}
                              (for [[scheme-key concepts-to-export] (sort-by first concepts-per-scheme)]
                                (let [base-name (name scheme-key)
                                      base-name-ttl (str base-name ".ttl")
                                      raw-filename (io/file ttl-dir base-name-ttl)]
                                  (logging/info "*** Output files related to" base-name "to" (str raw-filename))
                                  (io/make-parents raw-filename)
                                  (spit raw-filename (skos/render-turtle (produce-skos-data concept-map
                                                                                            concepts-to-export
                                                                                            (map collection-from-type types)
                                                                                            scheme-map)))
                                  [scheme-key {:full-filename (str raw-filename)
                                               :filename base-name-ttl}])))]
    (spit (io/file ttl-dir "README") (utils/flatten-lines
                                      ["# SKOS files on Turtle format"
                                       ""
                                       "This directory contains the raw export to [SKOS](https://www.w3.org/2004/02/skos/) of the JobTech Taxonomy to the [Turtle format](https://www.w3.org/TR/turtle/)."
                                       ""
                                       "| Filename | Concept types |"
                                       "|---|---|"
                                       (for [[scheme-key {:keys [filename]}] scheme-file-map]
                                         (format "| %s | %s |"
                                                 (utils/md-link filename filename)
                                                 (cljstr/join ", " (map name (sort (concept-types-per-scheme scheme-key))))))]))
    (spit (io/file raw-dir "README")
          (utils/flatten-lines
           ["# SKOS export on various formats"
            ""
            "Currently, only export to the [Turtle (ttl)](https://www.w3.org/TR/turtle/) format is supported."]))
    scheme-file-map))



(comment
  (do

    (defn load-json [filename]
      (-> filename
          slurp
          (cheshire/parse-string true)
          vals
          first
          vals
          first))
    
    (def concepts (load-json "build/result/begrepp-och-vanliga-relationer.json"))
    (def types (load-json "build/result/begreppstyper.json"))

    (defn demo []
      (write-skos-files "/tmp/t2s" concepts types))

    (def result (demo))

    ;;(spit "/tmp/taxonomy.ttl" (skos/render-turtle result))
    ;;(println "Done")

    
    ))
