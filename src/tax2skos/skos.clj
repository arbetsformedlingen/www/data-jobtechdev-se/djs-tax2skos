(ns tax2skos.skos
  (:require [clojure.spec.alpha :as spec]
            [clojure.string :as cljstr])
  (:import [java.net URI]))

;; https://www.w3.org/TR/turtle/

(defn unqualified-keyword? [x]
  (and (keyword? x)
       (not (qualified-keyword? x))))

(spec/def ::uri #(instance? URI %))

(spec/def ::ref (spec/or :uri ::uri
                         :key keyword?))

(spec/def ::prefix (spec/cat :prefix #{:prefix}
                             :name unqualified-keyword?
                             :uri ::uri))
(spec/def ::base (spec/cat :prefix #{:base}
                           :uri ::uri))

(spec/def ::lang-string (spec/cat :value string?
                                  :lang #(and (keyword? %)
                                              (= "lang" (namespace %)))))

(spec/def ::triplet (spec/cat :prefix #{:triplet}
                              :src ::ref
                              :rel keyword?
                              :dst (spec/or :value string?
                                            :lang-string ::lang-string
                                            :ref ::ref)))

(spec/def ::data (spec/coll-of (spec/or :prefix ::prefix
                                        :base ::base
                                        :triplet ::triplet)))

(defn render-key [x]
  (if-let [n (namespace x)]
    (str (namespace x) ":" (name x))
    (name x)))

(defn render-uri [x]
  (str "<" x ">"))

(defn render-ref [[t x]]
  (case t
    :key (render-key x)
    :uri (render-uri x)))




(def other-chars (set " ,.!:;"))

(defn valid-char? [x]
  (let [xi (int x)]
    (or (Character/isAlphabetic xi)
        (Character/isDigit xi)
        (other-chars x))))

(defn esc [s]
  (str "\\" s))

(def replacements {\tab (esc "t")
                   \backspace (esc "b")
                   \newline (esc "n")
                   \return (esc "r")
                   \formfeed (esc "f")
                   \" (esc "\"")
                   \\ (esc "\\")
                   (char 160) " "})

(defn clean-char [x]
  (or ;(if (valid-char? x) x)
      (get replacements x)
      x))

(defn clean-chars [x]
  (into []
        (map clean-char)
        x))

(defn render-string-literal [x]
  (str "\"" (cljstr/trim (apply str (clean-chars (cljstr/trim x))))  "\""))

(defn render-obj [[t x]]
  (case t
    :value (render-string-literal x)
    :lang-string (str (render-string-literal (:value x)) "@" (name (:lang x)))
    :ref (render-ref x)))

(defn render-turtle
  ([data] (render-turtle data {}))
  ([data prop-info]
   (let [cdata (spec/conform ::data data)
         _ (do (when (= ::spec/invalid cdata)
                 (throw (ex-info "Invalid data" {:data data
                                                 :explanation
                                                 (spec/explain-str
                                                  ::data data)}))))
         triplets (for [[t x] cdata :when (= t :triplet)] x)
         triplet-groups (sort-by key (group-by :src triplets))]
     (cljstr/join
      "\n"
      (into []
            (comp cat cat)
            [;; Output the header data
             [(for [[t item] cdata :when (not= t :triplet)]
                (case t
                  :base (str "@base " (render-uri (:uri item)) " .")
                  :prefix (str "@prefix " (name (:name item)) ": " (render-uri (:uri item)) " .")))]

             ;; Output the concepts ordered by id.
             (for [[concept-key props] triplet-groups]
               (for [[i prop] (map-indexed vector (sort-by #(get prop-info (:rel %) 100) props))]
                 (str (if (zero? i) (render-ref (:src prop)) "  ")
                      " "
                      (render-key (:rel prop))
                      " "
                      (render-obj (:dst prop))
                      " "
                      (if (= (inc i) (count props)) "." ";"))))])))))
