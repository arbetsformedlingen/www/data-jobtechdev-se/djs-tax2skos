(ns tax2skos.dataset-spec
  (:require [clojure.string :as cljstr]
            [clojure.data.csv :as csv]
            [clojure.java.io :as io]
            [tax2skos.graphql :as graphql]
            [clojure.spec.alpha :as spec]))



;;;------- Sanity checking -------

(def col-name-key-sv (keyword "dcat:Dataset \"sv\""))
(def col-name-key-en (keyword "dcat:Dataset \"en\""))
(def col-description-sv (keyword "dcterms:description \"sv\""))
(def col-description-en (keyword "dcterms:description \"en\""))
(def col-keyword-sv (keyword "dcat:keyword"))
(def col-accrual-periodicity (keyword "dcterms:accrualPeriodicity"))
(def col-query (keyword "query"))
(def col-url (keyword "url"))
(def col-landing-page (keyword "dcat:landingPage"))

(defn valid-excel-row? [x]
  (and (map? x)
       (seq (get x col-name-key-sv))
       (seq (get x col-url))))

(defn valid-row? [x]
  (and (map? x)
       (seq (get x col-name-key-sv))
       (seq (get x col-query))))

(defn check-data [data]
  (when-not (map? data)
    (throw (ex-info "Not a map")))
  (let [column-set (set (:columns data))]
    (doseq [c [col-name-key-sv col-query]]
      (when-not (column-set c)
        (throw (ex-info "Missing column" {:column c})))))
  (doseq [row (:data data)]
    (when (not (valid-row? row))
      (throw (ex-info "Invalid query row" {:row row}))))
  data)

(defn remove-newlines [s]
  (cljstr/replace s "\n" " "))

;;;------- CSV input/output -------

(defn export-csv-spec [filename data]
  (check-data data)
  (let [columns (:columns data)]
    (with-open [w (io/writer filename)]
      (csv/write-csv w (into [(mapv name columns)]
                             (for [row (:data data)]
                               (for [k columns]
                                 (str (get row k)))))))))



(defn import-csv-spec
  "This function imports a CSV file on the RFC4180 format where every row is a GraphQL query along with metadata."
  [filename-or-url]
  (let [[header-labels & rows] (with-open [r (io/reader filename-or-url)]
                          (doall (csv/read-csv r)))
        header (map keyword header-labels)]
    (check-data {:columns header
                 :data (for [row rows]
                         (zipmap header row))})))

(spec/def ::query graphql/parsable?)
(spec/def ::data (spec/coll-of (spec/keys :req-un [::query])))
(spec/def ::dataset-spec (spec/keys :req-un [::data]))

(defn validate-dataset-spec [data]
  (when-not (spec/valid? ::dataset-spec data)
    (throw (ex-info (str "Invalid dataset: "
                         (spec/explain-str ::dataset-spec data))
                    {})))
  data)

(comment
  (do

    (def data (import-csv-spec "taxonomy-dataset-3.csv"))


    ))
