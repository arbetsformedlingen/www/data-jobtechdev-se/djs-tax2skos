(ns tax2skos.api
  (:require [tax2skos.dataset-downloader :as dataset-downloader]
            [cheshire.core :as cheshire]
            [clojure.edn :as edn]
            [tax2skos.core :as tax2skos]
            [clojure.java.io :as io]
            [tax2skos.utils :as utils]
            [tax2skos.aws-interop :as aws-interop]
            [clojure.tools.logging :as logging])
  (:import [java.net URI])
  (:gen-class))

(def logfile-varname "TAX2SKOS_LOGFILE")
(def root-uri (URI. "https://data.jobtechdev.se/taxonomy/"))



(defn- load-json [filename]
  (-> filename
      slurp
      (cheshire/parse-string true)))

(defn- unwrap-data [qjson]
  (-> qjson
      vals
      first
      vals
      first))

(defn- derive-files-for-version [{:keys [version
                                         dir
                                         data-subsets] :as version-data}
                                 {:keys [debug-output-root] :as setup}]
  {:pre [version
         dir
         (sequential? data-subsets)
         debug-output-root]}
  (logging/info (format "Derive files for version %d to directory %s" version dir))
  (let [dir (io/file dir)
        filemap (into {} (map (juxt :filename identity)) data-subsets)
        concepts-file (filemap "concepts-and-common-relations.json")
        types-file (filemap "concept-types.json")]

    (when (not concepts-file)
      (throw (ex-info "Missing concepts file" {:version-data version-data
                                               :setup setup})))

    (when (not types-file)
      (throw (ex-info "Missing types file" {:version-data version-data
                                            :setup setup})))

    ;; Here we perform the actual export
    (let [all-concepts (-> concepts-file :full-filename load-json unwrap-data)
          all-types (-> types-file :full-filename load-json unwrap-data)

          ;; Export to SKOS
          skos-dir (io/file dir "skos")
          filemap (tax2skos/write-skos-files
                   skos-dir
                   all-concepts
                   all-types)]
      
      (spit (io/file skos-dir "README")
            (utils/flatten-lines
             [(str "# SKOS export of JobTech Taxonomy")
              ""
              (format
               "This directory contains the [SKOS](https://www.w3.org/2004/02/skos/) export of the JobTech Taxonomy version %s."
               (str version))]))
      
      (spit (io/file dir "README")
            (utils/flatten-lines
             ["# JobTech Taxonomy"
              ""
              (format "This directory contains the JobTech Taxonomy version %s on the following formats:" (str version))
              ""
              "| Format | Description |"
              "|---|---|"
              "| [query](query) | Responses on **json** format of various GraphQL queries |"
              "| [skos](skos) | SKOS export of derived from the data in [query](query). |"]))
      {:metadata {:version version
                  :dir (str dir)
                  :skos-dir (str skos-dir)
                  :skos-files filemap}
       :concept-count (count all-concepts)
       :version-data version-data
       })))

(defn derive-files
  "Convert the json file with the full taxonomy to other representations such as SKOS or generated source code for various languages."
  [{:keys [index-filename derived-index-filename debug-output-root output-root] :as config}]
  {:pre [(string? index-filename)
         (string? derived-index-filename)
         (string? debug-output-root)
         (string? output-root)]}
  (let [downloaded-files (load-json index-filename)
        dcat-template (dataset-downloader/load-dcat-template
                       (merge dataset-downloader/default-dcat-config config))
        results (vec (for [filedata-for-version (sort-by :version downloaded-files)]
                       (do
                         (logging/info "Produce SKOS for version" (:version filedata-for-version))
                         (derive-files-for-version
                          filedata-for-version
                          {:debug-output-root debug-output-root}))))
        version-result-map (into {} (map (juxt (comp :version :metadata) identity)) results)
        [_max-version max-data] (apply max-key first version-result-map)
        max-dir (-> max-data :metadata :dir io/file)
        latest-version-dir (-> max-dir .getParent (io/file "latest"))
        latest-data (assoc-in max-data [:metadata :dir] latest-version-dir)]

    ;; Copy the directory of the max version to "latest"
    (utils/copy-dir max-dir latest-version-dir)

    ;; Produce dcat files for the latest version
    (let [version-data (:version-data latest-data)]
      (dataset-downloader/produce-dcat (merge version-data
                                              {:dcat-template dcat-template
                                               :src-path max-dir
                                               :dst-path latest-version-dir
                                               :root-uri root-uri
                                               :output-root output-root
                                               :version-debug-output-root (->> version-data
                                                                               :dir
                                                                               (io/file debug-output-root))})))


    ;; Produce top-level readmes.
    (doseq [readme-root [(io/file output-root) (io/file output-root "version")]]
      (spit (io/file readme-root "README")
            (utils/flatten-lines
             ["# JobTech Taxonomy version listing"
              ""
              "This directory contains the following version of JobTech taxonomy:"
              ""
              "| Version | Subdirectory | # concepts |"
              "|---|---|---|"
              (for [block [results [latest-data]]
                    {:keys [metadata concept-count]} block
                    :let [relpath (str (.relativize (.toPath readme-root)
                                                    (.toPath (io/file (:dir metadata)))))]]
                (format "| %s | %s | %d |"
                        (str (:version metadata))
                        (utils/md-link (format "`%s`" relpath) relpath)
                        concept-count))])))

    (io/make-parents derived-index-filename)
    (spit derived-index-filename (utils/json-string (map :metadata results)))
    (logging/info "Done deriving files")))

(def downloader-config dataset-downloader/default-config)


(defn download-all-datasets [args]
  (dataset-downloader/download-all-datasets
   args
   downloader-config))

(def fetch-published-versions aws-interop/fetch-published-versions)

(defn map-from-edn-kv-pairs [kv-pairs]
  (into {}
        (comp (map edn/read-string)
              (partition-all 2))
        kv-pairs))

(defmacro functions-cli [& functions]
  `(fn [cmd# & kv-pairs#]
     ((case cmd#
        ~@(mapcat (fn [f] [(name f) f]) functions))
      (map-from-edn-kv-pairs kv-pairs#))))

(def -main (functions-cli download-all-datasets
                          derive-files
                          fetch-published-versions))


(comment
  (do


    (download-all-datasets {:spec-filename "taxonomy-dataset-3.csv"
                            :output-root "build/result"
                            :index-filename "build/download_index.json"
                            :published-versions-file "build/published_versions.json"})

    (derive-files {:index-filename "build/download_index.json"
                   :derived-index-filename "build/derived_index.json"
                   :debug-output-root "build/debug"
                   :output-root "build/result"})

    ))
