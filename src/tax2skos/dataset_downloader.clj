(ns tax2skos.dataset-downloader
  (:require [tax2skos.dataset-spec :as dataset-spec]
            [clojure.java.io :as io]
            [clojure.string :as cljstr]
            [cheshire.core :as cheshire]
            [tax2skos.graphql-request :as graphql-request]
            [tax2skos.utils :as utils]
            [clojure.tools.logging :as logging]
            [tax2skos.dcat :refer [list-dcat-issues]])
  (:import [java.nio.file Path]
           [java.io File]
           [java.net URI]))


;; https://stackoverflow.com/questions/1436761/turn-off-apache-common-logging

;;(def dcat-processor-application-properties (ApplicationProperties.))

;; See this
;; https://stackoverflow.com/questions/55036718/how-to-import-spring-boot-jar-into-other-spring-boot-jar-maven-base

(defn filename-formatted-name [s]
  (-> s
      cljstr/lower-case
      (cljstr/replace " " "-")))

(def load-json (comp cheshire/parse-string slurp))

(def default-dcat-config
  {:dcat-template-file (io/resource "dcat_template.json")
   :dcat-catalog-file (io/resource "catalog.json")})

(defn load-dcat-template [{:keys [dcat-template-file dcat-catalog-file]}]
  {:pre [dcat-template-file
         dcat-catalog-file]}
  (logging/infof "Load DCAT template from template '%s' and catalog '%s'"
                 (str dcat-template-file)
                 (str dcat-catalog-file))
  (-> dcat-template-file
      load-json
      (update-in ["info" "x-dcat"]
                 merge
                 (load-json dcat-catalog-file))))


(defn normalize-path [x]
  (cond
    (instance? Path x) x
    (instance? File x) (.toPath x)
    (string? x) (-> x io/file .toPath)
    :else (throw (ex-info "Cannot make path" {:value x}))))

(defn published-uri-for-local-file [base-uri root-path filename]
  {:pre [(instance? URI base-uri)]}
  (let [root-path (normalize-path root-path)
        filename (normalize-path filename)
        rel (.relativize root-path filename)]
    (.resolve base-uri (str rel))))

(def default-accrual-periodicity "http://publications.europa.eu/resource/authority/frequency/UNKNOWN")
(def default-landing-page "atlas.jobtechdev.se")
(def default-keywords "Taxonomi, Jobtech, Arbetsförmedlingen")

(defn job-name-data [job]
  (let [mangled-name (filename-formatted-name (job dataset-spec/col-name-key-en))
        base-filename (str mangled-name ".json")
        dcat-filename (str mangled-name ".json.dcat.json")]
    {:mangled-name mangled-name
     :json-filename base-filename
     :dcat-filename dcat-filename}))

(def dcat-dataset-path ["info" "x-dcat" "dcat-dataset"])

(defn string-kind-fn [x]
  (when (string? x) :string))

(defn acc-error [error-list value error-message]
  [(conj error-list error-message)
   value])

(defn acc-result [error-list value]
  [error-list value])

(defn empty-string-message [x]
  (when (empty? x)
    "Empty string value"))

(defn sanitize-value [error-list label pred-str-fn input-value default-value]
  (if-let [err (pred-str-fn input-value)]
    (acc-error error-list
               default-value
               {:message (format "Sanity check: '%s' for value '%s', replacing by default."
                                 err label)})
    (acc-result error-list input-value)))

(defn sanitize-values [pred-str-fn value-map]
  (reduce
   (fn [[error-list result] [k [actual-value sane-default]]]
     (let [[error-list sanitized] (sanitize-value error-list
                                                  (name k)
                                                  pred-str-fn
                                                  actual-value
                                                  sane-default)]
       [error-list (assoc result k sanitized)]))
   [[] {}]
   value-map))

(defn job-dcat
  "Given a `template` DCAT data structure and the specification `job` for a file in the dataset, 
return a datastructure that has the format of `template` but populated with values from `job`."
  [{:keys [dcat-template root-uri output-root]} {:keys [job output-dir]}]
  {:pre [dcat-template root-uri output-root job]}
  (let [name-data (job-name-data job)
        title-sv (job dataset-spec/col-name-key-sv)
        url (->> name-data
                 :json-filename
                 (io/file output-dir)
                 (published-uri-for-local-file root-uri output-root)
                 str)
        [error-list {:keys [accrual-periodicity
                            landing-page
                            desc-sv
                            desc-en
                            keywords
                            title-en]}] (sanitize-values
                                         empty-string-message
                                         {:accrual-periodicity
                                          [(cljstr/trim (job dataset-spec/col-accrual-periodicity))
                                           default-accrual-periodicity]
                                          :landing-page
                                          [(job dataset-spec/col-landing-page)
                                           default-landing-page]
                                          :desc-sv
                                          [(job dataset-spec/col-description-sv)
                                           "Beskrivning saknas"]
                                          :desc-en
                                          [(job dataset-spec/col-description-en)
                                           "No description"]
                                          :keywords
                                          [(job dataset-spec/col-keyword-sv)
                                           default-keywords]
                                          :title-en
                                          [(job dataset-spec/col-name-key-en)
                                           (str "Swedish title (not yet in English): " title-sv)]})
        basic-info {"title-sv" title-sv
                    "title-en" title-en
                    "title" title-en
                    "description-sv" desc-sv
                    "description-en" desc-en}]
    [error-list
     (update-in dcat-template
                dcat-dataset-path
                #(utils/merge-into-structure
                  string-kind-fn
                  %
                  (merge basic-info
                         {"about" (str url "about0")}
                         {"keyword-sv" keywords
                                        ;"keyword-en" ""
                          "landingPage" landing-page
                          "accrualPeriodicity" accrual-periodicity
                          "distribution" (merge basic-info
                                                {"accessURL" url
                                                 "downloadURL" url
                                                 "about" (str url "#about1")})})))]))



(defn- format-dcat-error-group [error-group]
  (let [[{:keys [mangled-name
                 debug-filename]}] error-group]
    [(format "* %s:" mangled-name)
     (for [error error-group]
       [(format "- %s" (:message error))
        [(when-let [details (:details error)]
           (str "Details: " details))]])
     (when debug-filename
       (str " Output file: " debug-filename))]))

(defn summarize-dcat-errors
  "Given a list of maps with `:mangled-name` and `:message` keys, produce a pretty-printed string for errors about dcat."
  [dcat-errors0]
  (let [dcat-errors (remove empty? dcat-errors0)
        error-count (count (into [] cat dcat-errors))
        error-msg (utils/render-indented
                   (if (zero? error-count)
                     ["All dcat files look OK!"]
                     [(format "%d DCAT ERRORS:" error-count)
                      ""
                      (for [errors dcat-errors]
                        (format-dcat-error-group errors))]))]
    [error-count error-msg]))

(def success? #{:success "success"})

(defn produce-data-subset-dcat
  "Produce a dcat file for a specific downloaded file."

  [{:keys [src-path dst-path version-debug-output-root] :as setup}
   {:keys [full-filename job status output-dir] :as file-info}]
  {:pre [full-filename
         (map? job)
         status
         src-path
         dst-path
         version-debug-output-root]}
  (assert (string? (:full-filename file-info)))
  
  (let [_ (logging/info "dcat:" (job dataset-spec/col-name-key-sv))
        {:keys [mangled-name dcat-filename]} (job-name-data job)]
    (if (success? status)
      (let [ ;; Produce dcat data by populating a template with and cleaning it from line breaks.
            ;; Why do we need to remove line breaks?
            [job-errors full-dcat] (->> file-info
                                        (job-dcat setup)
                                        (utils/clean-strings-recursively utils/remove-line-breaks))

            ;; Analyze the dcat data and output debug data to files.
            [base-info dcat-errors] (list-dcat-issues mangled-name full-dcat version-debug-output-root)
            full-dcat-filename (utils/change-path-root src-path
                                                       dst-path
                                                       (io/file output-dir dcat-filename))]

        ;; Render the dcat as json in the directory of the file that it specifies.
        (->> (get-in full-dcat dcat-dataset-path)
             utils/json-string
             (spit full-dcat-filename))

        (logging/info "Produced" (str full-dcat-filename))

        ;; Collect/flatten all the errors (if any)
        (into []
              (comp cat (map (partial merge base-info)))
              [job-errors dcat-errors]))
      
      (do (logging/warn "Download failed: no dcat to output.")

          [{:message "Failed to download json file"
            :mangled-name mangled-name}]))))

(defn produce-dcat [{:keys [data-subsets version-debug-output-root] :as setup}]
  {:pre [data-subsets]}
  (let [errors (vec (for [file-info data-subsets]
                      (produce-data-subset-dcat setup file-info)))
        [error-count summary] (summarize-dcat-errors errors)
        logfile (io/file version-debug-output-root "dcat.log")]
    (if (pos? error-count)
      (logging/error summary)
      (logging/info summary))
    (io/make-parents logfile)
    (spit logfile summary)))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;; D O W N L O A D   D A T A S E T S
;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


(defn get-versions
  "Query the available taxonomy versions from the GraphQL API."
  [graphql-request-config]
  (let [{:keys [status data]} (graphql-request/run-query
                               "query MyQuery {versions {id}}"
                               graphql-request-config)]
    (if (= :success status)
      (let [versions (sort (map :id (get-in data [:data :versions])))]
        (logging/info "Taxonomy versions:" versions)
        versions)
      (do (logging/error "No taxonomy versions.")
          nil))))

(defn render-query-info-page [columns api-url filename job]
  (utils/flatten-lines
   [(str "# Description of [" filename "](" filename ")")
    ""
    (format "The file [%s](%s) was produced from the response of sending the GraphQL request"
            filename filename)
    ""
    "```"
    (job dataset-spec/col-query)
    "```"
    ""
    "| Property | Value |"
    "|---|---|"
    (for [colkey columns]
      (str "| `" (name colkey) "` | `" (job colkey) "` |"))]))

(defn download-data-subset
  "Download a single file the dataset and return data about the downloaded file."
  [{:keys [columns]} job version query-dir
   {:keys [graphql-request-config
           page-size
           retry-count-no-pagination]}]
  
  {:pre [(map? job)
         (map? graphql-request-config)
         (number? page-size)
         (utils/file? query-dir)]}
  (logging/info "----" (job dataset-spec/col-name-key-sv) "for version" version)
  (let [ ;;{:keys [version-root query-root]} (dirs-for-version output-root version)
        query-str (job dataset-spec/col-query)
        base-config (merge graphql-request-config {:version version})
        configs-to-try [(merge base-config
                               {:retry-count retry-count-no-pagination})
                        (merge base-config
                               {:page-size page-size})]
        {:keys [status data]} (graphql-request/first-successful-parameterized-query
                               query-str configs-to-try)
        {:keys [mangled-name json-filename]} (job-name-data job)
        output-dir (io/file query-dir mangled-name)
        full-json-filename (io/file output-dir json-filename)
        info-page (render-query-info-page columns (:address base-config) json-filename job)]
    (merge
     {:filename (str json-filename)
      :full-filename (str full-json-filename)
      :version version
      :job job
      :subdir-name mangled-name
      :output-dir (str output-dir)}
     (if (success? status)
       (do
         (io/make-parents full-json-filename)
         (spit full-json-filename (utils/json-string data))
         (spit (io/file output-dir "README") info-page)
         (utils/success (job dataset-spec/col-description-sv)))
       (utils/failure (str "**Failed to produce file:**\n```\n"
                           data
                           "\n```\n"))))))

(defn render-data-subsets-info-page [version outcomes]
  (utils/flatten-lines
   [(format "# Data subsets for version %s of JobTech Taxonomy" (str version))
    ""
    "| Name (en) | Name (sv) | Description (en) |"
    "|---|---|---|"
    (for [outcome outcomes]
      (let [job (:job outcome)
            href (str (:subdir-name outcome) "/")]
        (str "| "
             (utils/md-link (job dataset-spec/col-name-key-en) href)
             " | "
             (utils/md-link (job dataset-spec/col-name-key-sv) href)
             " | *"
             (job dataset-spec/col-description-en)
             "* |")))]))

(defn- download-datasets-for-version [version dataset-spec config query-dir]
  (vec (for [job (:data dataset-spec)]
         (download-data-subset dataset-spec
                               job
                               version
                               query-dir
                               config))))

(defn download-datasets
  "Given a list of `versions` and a `dataset-spec`icfication listing the files to be downloaded,
 download all files for all versions to `output-root` and also output a list with data about all 
downloaded files to `index-filename`."
  [versions dataset-spec config output-root index-filename]
  {:pre [(every? integer? versions)]}
  (let [outcomes (vec (for [version versions]
                        (let [version-dir (io/file output-root "version" (str version))
                              query-dir (io/file version-dir "query")
                              outcomes (download-datasets-for-version
                                        version dataset-spec config query-dir)]
                          (spit (io/file query-dir "README")
                                (render-data-subsets-info-page version outcomes))
                          {:version version
                           :dir (str version-dir)
                           :data-subsets outcomes})))]
    ;; Write a file with a list of all files that were produced.
    (io/make-parents index-filename)
    (spit index-filename (utils/json-string outcomes))))

(def default-config {:graphql-request-config graphql-request/default-config
                     :page-size 100
                     :retry-count-no-pagination 1
                     :max-version-count-to-download 2})

(defn parse-version [version-str]
  {:pre [(string? version-str)]}
  (try
    (Long/parseLong version-str)
    (catch NumberFormatException _e
      nil)))

(defn load-published-versions [filename]
  {:pre [(string? filename)]}
  (let [file (io/file filename)]
    (when (.exists file)
      (->> filename
           slurp
           cheshire/parse-string
           (keep parse-version)
           sort))))

(defn latest-version-seq-that-has-not-been-published-yet [published-versions available-versions]
  (let [available-versions (sort-by - available-versions)
        [first-not-published] (remove (set published-versions) available-versions)]
    (if first-not-published
      [first-not-published]

      ;; TODO:
      ;; By default, if everything has been published,
      ;; we still publish the latest available version,
      ;; just in case :-). But we could also return an
      ;; empty array here if we trust the latest published
      ;; data to be OK.
      (take 1 available-versions))))

(defn download-all-datasets
  "Top-level API function for downloading all the files for all or only the last version."
  [{:keys [spec-filename output-root index-filename published-versions-file]} config]
  {:pre [(string? spec-filename)
         (string? output-root)
         (string? index-filename)]}
  (logging/info "Download all datasets from" spec-filename)
  (when-let [available-versions (get-versions (:graphql-request-config config))]
    (let [dataset-spec (dataset-spec/validate-dataset-spec
                        (dataset-spec/import-csv-spec spec-filename))
          published-versions (load-published-versions published-versions-file)
          versions (latest-version-seq-that-has-not-been-published-yet published-versions available-versions)]
      (logging/info "Previously published versions:" published-versions)
      (logging/info "Available versions:" available-versions)
      (logging/info "Versions to download:" versions)
      (download-datasets versions
                         dataset-spec
                         config
                         output-root
                         index-filename))))

;; This is what we pass to download 
(def default-dev-args {:spec-filename "taxonomy-dataset-3.csv"
                       :output-root "build/result"
                       :dst-directory "build/result/latest"
                       :index-filename "build/download_index.json"
                       :debug-output-root "build/debug/dcat"})

(comment
  (do

    (def published-versions (load-published-versions "build/published_versions.json"))

    (def available (get-versions (:graphql-request-config default-config)))


    ))

(comment
  (do


    (def specs (dataset-spec/import-csv-spec (:spec-filename default-dev-args)))

    ))

(comment
  (do
    
    (download-all-datasets default-dev-args default-config)

    ))

(comment
  (do

    (produce-dcat default-dev-args)

    ))

