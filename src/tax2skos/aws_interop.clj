(ns tax2skos.aws-interop
  (:require [clojure.string :as cljstr]
            [clojure.java.io :as io]
            [clojure.spec.alpha :as spec]
            [cheshire.core :as cheshire]
            [clojure.tools.logging :as logging])
  (:import [software.amazon.awssdk.services.s3 S3Client]
           [software.amazon.awssdk.services.s3.model ListObjectsV2Request DeleteObjectRequest]
           [software.amazon.awssdk.regions Region]
           [software.amazon.awssdk.auth.credentials

            AnonymousCredentialsProvider
            DefaultCredentialsProvider
            AwsCredentialsProviderChain
            AwsCredentialsProvider
            AwsBasicCredentials
            StaticCredentialsProvider
            ]))


;;

(def sample-config
  "[adhoc]
type = s3
access_key_id = ADHOC_ID_GOES_HERE
secret_access_key = ADHOC_PASSWORD_GOES_HERE
region = eu-central-1
[katt]
type = s3
access_key_id = katt
secret_access_key = KATT_PASSWORD_GOES_HERE")

(defn- parse-config-line-definition [s]
  (when-let [i (cljstr/index-of s "=")]
    (let [left (cljstr/trim (subs s 0 i))
          right (cljstr/trim (subs s (inc i)))]
      [:definition (keyword left) right])))

(def header-regex #"\s*\[(.*)\]\s*")

(defn- parse-config-line-header [s]
  (when-let [header (second (re-matches header-regex s))]
    [:header header]))

(defn- parse-config-line-empty [s]
  (let [s (cljstr/trim s)]
    (when (empty? s)
      [:empty s])))

(defn- parse-config-line [s]
  (or (parse-config-line-header s)
      (parse-config-line-definition s)
      (parse-config-line-empty s)
      (throw (ex-info "Failed to parse config line" {:line s}))))

(defn- build-sections [[result current-header] [line-type k v]]
  (case line-type
    :definition [(assoc-in result [current-header k] v) current-header]
    :header [result (keyword k)]
    :empty [result current-header]))

(defn parse-config [config-string]
  (->> config-string
       cljstr/split-lines
       (map parse-config-line)
       (reduce build-sections [{} nil])
       first))

(def config-sections [:default :adhoc])

(defn get-config-value [m k]
  (first (keep #(get-in m [% k]) config-sections)))


(defn credentials-provider [config]
  (let [access_key_id (get-config-value config :aws_access_key_id)
        secret_access_key (get-config-value config :aws_secret_access_key)]
    (AwsCredentialsProviderChain/of
     (into-array
      AwsCredentialsProvider
      (into []
            (remove nil?)
            [(when (and access_key_id secret_access_key)
               (StaticCredentialsProvider/create (AwsBasicCredentials/create access_key_id secret_access_key)))
             (DefaultCredentialsProvider/create)
             (AnonymousCredentialsProvider/create)])))))

(defn parse-bucket [bucket-str]
  (cljstr/trim bucket-str))

(def version-pattern #"taxonomy\/version\/(\w+)\/")

(defn version-from-path [path]
  (when-let [[_found-substring version] (re-find version-pattern path)]
    version))

(defn- first-existing-file [files]
  (or (first (for [filename files
                   :let [file (io/file filename)]
                   :when (.exists file)]
               file))
      (do (logging/error "None of these files exists:" files)
          nil)))

(defn load-and-merge-configs [config-files]
  (apply merge-with merge (for [filename (reverse config-files)
                                :let [file (io/file filename)]
                                :when (.exists file)]
                            (-> file slurp parse-config))))

(defn s3-connection [creds region-name]
  (.build (doto (S3Client/builder)
            (.region (Region/of region-name))
            (.credentialsProvider creds))))

(defn bucket-objects [connection bucket-name]
  (let [request (.build (doto (ListObjectsV2Request/builder)
                          (.bucket bucket-name)))]
    (into #{} (for [page (seq (.listObjectsV2Paginator connection request))
                    obj (.contents page)]
                obj))))

(defn list-s3-bucket-taxonomy-versions [bucket-name creds region-name]
  {:pre [(string? bucket-name)
         (string? region-name)]}
  (logging/info "List published taxonomy versions from" bucket-name "at" region-name)
  (try (with-open [connection (s3-connection creds region-name)]
         (into #{} (for [obj (bucket-objects connection bucket-name)
                         :let [filename (.key obj)
                               version (version-from-path filename)]
                         :when version]
                     version)))
       (catch software.amazon.awssdk.services.s3.model.S3Exception _
         (logging/error "Failed to access bucket at" bucket-name)
         nil)))

(defn- get-versions [bucket-files config-files]
  (let [bucket (when-let [bucket-file (first-existing-file bucket-files)]
                 (-> bucket-file slurp parse-bucket))
        config (load-and-merge-configs config-files)
        region (or (get-config-value config :region)
                   (do (logging/error "Missing region in config")
                       nil))]
    (logging/info "REGION:" region)
    (when (and bucket (seq config) region)
      (let [creds (credentials-provider config)]
        (list-s3-bucket-taxonomy-versions bucket creds region)))))

(defn fetch-published-versions [{:keys [bucket-files config-files output-file]}]
  {:pre [(sequential? bucket-files)
         (every? string? bucket-files)
         (string? output-file)]}
  (io/make-parents output-file)
  (->> (get-versions bucket-files config-files)
       cheshire/generate-string
       (spit output-file)))

;;;------- Useful functions -------

(spec/def ::path (spec/alt :taxonomy-versioned-file
                           (spec/cat :features #(and (:taxonomy %) (:versioned %))
                                     :prefix #{"taxonomy"}
                                     :version-root #{"version"}
                                     :version-key string?
                                     :rest (spec/* string?))))

(defn parse-path [path]
  (let [p (.toPath (io/file path))
        parts (map str p)
        [root-dir second-dir] parts
        features (into #{}
                       (remove nil?)
                       [(case root-dir
                          "taxonomy" :taxonomy
                          nil)
                        (case second-dir
                          "version" :versioned
                          nil)])
        conformed (spec/conform ::path (into [features] parts))
        filename (last parts)
        lower-filename (cljstr/lower-case filename)
        filetype (first (for [[k ending] [[:dcat-json ".dcat.json"]
                                          [:dcat-xml ".dcat.xml"]
                                          [:turtle ".ttl"]
                                          [:json ".json"]
                                          [:xml ".xml"]]
                              :when (cljstr/ends-with? lower-filename ending)]
                          k))
        version (when (not= ::spec/invalid conformed)
                  (let [[path-type path-data] conformed]
                    (case path-type
                      :taxonomy-versioned-file (:version-key path-data)
                      nil)))]

    {:features features
     :filetype filetype
     :parts parts
     :path path
     :version version
     :conformed conformed}))

(defn dcat-file-to-remove? [{:keys [features filetype version]}]
  (and (:taxonomy features)
       (#{:dcat-json :dcat-xml} filetype)
       (not= "latest" version)))

(defn delete-old-dcat-files [bucket-name creds region-name]
  {:pre [(string? bucket-name)
         (string? region-name)]}
  (with-open [^S3Client connection (s3-connection creds region-name)]
    (let [ks (for [object (bucket-objects connection bucket-name)
                   :let [k (.key object)
                         parsed (parse-path k)]
                   :when (dcat-file-to-remove? parsed)]
               k)]
      (doseq [k ks]
        (logging/info "Delete" k)
        (.deleteObject
         connection
         (.build (doto (DeleteObjectRequest/builder)
                   (.bucket bucket-name)
                   (.key k)))))
      (logging/info "\n\n\n****** Deleted" (count ks) "objects"))))




(comment

  (do

    (def bucket-name "data.jobtechdev.se-adhoc-test")
    (def region (-> config :default :region))

    #_(def region "eu-north-1")
    #_(def bucket-name "data.jobtechdev.se-adhoc")
    
    
    (def config (load-and-merge-configs ["/home/jonas/.aws/config" "/home/jonas/.aws/credentials"]))
    (def creds (credentials-provider config))
    

    (def files (with-open [con (s3-connection creds region)]
                 (bucket-objects con bucket-name)))

    )

  (do

    (delete-old-dcat-files "data.jobtechdev.se-adhoc-test"
                           creds
                           "eu-central-1")
    )

  (do

    (delete-old-dcat-files "data.jobtechdev.se-adhoc"
                           creds
                           "eu-north-1")

    )


  (do
    
    (def parsed-files (keep parse-path (for [f files]
                                         (.key f))))

    (def other-versioned-files (remove (comp #{nil "latest"} :version) parsed-files))
    
    (def dcat-files (filter dcat-file-to-remove? parsed-files))

    (into #{} (map :version) dcat-files)

    )

  
  
  (def versions (list-s3-bucket-taxonomy-versions bucket-name))
  
  (do
    

    (def r (.build (doto (ListObjectsV2Request/builder)
                     (.bucket "data.jobtechdev.se-adhoc-test"))))
    
    (def c (.build (S3Client/builder)))

    (def pages (.listObjectsV2Paginator c r))

    (def objs (vec (take 10 (for [page (seq pages)
                                  obj (.contents page)]
                              obj))))

    (def obj (first objs))

    (doseq [f (.sdkFields obj)]
      (println "Membername:" (.memberName f)))

    (println (.key obj))
    
    ))
