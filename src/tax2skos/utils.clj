(ns tax2skos.utils
  (:require [clojure.walk :as walk]
            [clojure.string :as cljstr]
            [clojure.tools.logging :as logging]
            [cheshire.core :as cheshire]
            [clojure.java.io :as io])
  (:import [java.io File]))

(declare merge-into-structure)

(defn- merge-into-sub [kind-fn dst [k v]]
  (assoc dst k (merge-into-structure kind-fn (get dst k) v)))

(defn merge-into-structure [kind-fn a b]
  (if (and (map? a) (map? b))
    (reduce (partial merge-into-sub kind-fn) a b)
    (let [ak (kind-fn a)
          bk (kind-fn b)]
      (if (and (= ak bk) (some? ak))
        b
        (throw (ex-info "Failed to merge values" {:a a :b b}))))))

(declare sort-all-unordered)

(defn sort-all-unordered [x]
  (cond
    (map? x) (into (sorted-map) (map (fn [[k v]] [k (sort-all-unordered v)])) x)
    (set? x) (into (sorted-set) (map sort-all-unordered) x)
    (sequential? x) (let [x (map sort-all-unordered x)]
                      (cond
                        (every? #(and (map? %) (contains? % :id)) x) (vec (sort-by :id x))
                        (every? string? x) (vec (sort x))
                        :default x))
    :default x))

(defn clean-strings-recursively [string-cleaner data]
  (walk/postwalk #(if (string? %)
                    (string-cleaner %)
                    %)
                 data))

(defn remove-line-breaks [s]
  (cljstr/escape s {\newline " "
                    \return " "}))

(defn json-string [x]
  (-> x
      sort-all-unordered
      cheshire/generate-string))

(defn format-important [s]
  (str "!!! " (cljstr/join " " (vec s)) " !!!"))

(defn- render-indented-sub [prefix dst x]
  (cond
    (nil? x) dst
    
    (coll? x) 
    (reduce (partial render-indented-sub (str prefix "  "))
            dst
            x)

    :default
    (str dst "\n" prefix x)))

(defn render-indented [x]
  (reduce (partial render-indented-sub "") "" x))

(defn failure [message]
  (logging/error "Failure:" message)
  {:status :failure
   :message message})

(defn success
  ([message]
   (success message nil))
  ([message data]
   (logging/info "Success:" message)
   {:status :success
    :message message
    :data data}))

(defn file? [x]
  (instance? java.io.File x))

(defn flatten-lines [input]
  (if (string? input)
    input
    (cljstr/join "\n" (map flatten-lines input))))

(defn md-link
  ([label-and-target]
   (md-link label-and-target label-and-target))
  ([label target]
   (format "[%s](%s)" label target)))

(defn change-path-root [src-root0 dst-root0 src-file]
  (let [src-root (.toPath src-root0)
        dst-root (.toPath dst-root0)
        src-path (.toPath src-file)
        rel (.relativize src-root src-path) ;; Path from src-root to src-path (direction -->)
        dst-path (.resolve dst-root rel)
        dst-file (.toFile dst-path)]
    dst-file))

(defn copy-dir [src-dir dst-dir]
  (logging/info "Copy" src-dir "to" dst-dir)
  (let [src-dir (io/file src-dir)
        dst-dir (io/file dst-dir)]
    (doseq [src-file (file-seq (io/file src-dir)) :when (not (.isDirectory src-file))]
      (let [dst-file (change-path-root src-dir dst-dir src-file)]
        (logging/debug "Copy from" (str src-file) "to" (str dst-file))
        (io/make-parents dst-file)
        (io/copy src-file dst-file)))))
