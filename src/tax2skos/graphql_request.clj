(ns tax2skos.graphql-request
  (:require [clj-http.client :as client]
            [cheshire.core :as cheshire]
            [clojure.tools.logging :as logging]
            [tax2skos.graphql :as graphql]
            [clojure.spec.alpha :as spec]))

;; IMPORTANT:
;;
;; If you get status 504 back from the server in OpenShift, try increasing the
;; timeout of the route. Read more about that here:
;; https://access.redhat.com/documentation/ja-jp/red_hat_process_automation_manager/7.1/html/managing_and_monitoring_process_server/configuring-openshift-connection-timeout-proc

;; Convention in this file: *use return values over exceptions*. Many of the errors here are expected and should probably be dealt with.

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;; B A S I C   Q U E R I E S
;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(def default-config {;; Todo: This URL should depend on the environment where we run the infrastructure, and hardcoding the URL should
                     ;; probably only be a fallback.
                     ;; There is an issue for that: https://gitlab.com/arbetsformedlingen/taxonomy-dev/backend/jobtech-taxonomy-api/-/issues/568
                     :address "http://api-jobtech-taxonomy-api-prod-read.prod.services.jtech.se/v1/taxonomy/graphql"
                     
                     :connection-timeout 20000
                     :retry-count 6})

;; client/get
;; 1. Unhandled org.apache.http.NoHttpResponseException
;;    api-prod-taxonomy-api-gitops.prod.services.jtech.se:80 failed to
;;    respond

(defn retry-loop [request-fn result-fn retry-count]
  (loop [responses []]
    (if (= (count responses) retry-count)
      (do (logging/error "No attempt succeeded.")
          {:status :failure
           :data responses})
      (let [response (request-fn)]
        (logging/debug "Got response")
        (if (= 200 (:status response))
          (do (when (seq responses)
                (logging/info "Succeeded after" (inc (count responses)) "attempts."))
              {:status :success
               :data (result-fn response)})
          (do (logging/warn (format "Query failed (%s), retrying" (str (:status response))))
              (recur (conj responses response))))))))

(defn run-query [query {:keys [address retry-count connection-timeout]}]
  (retry-loop #(try
                 (logging/debug "Request GET" address)
                 (client/get
                  address
                  {:accept :json
                   :query-params {"query" query}
                   :connection-timeout connection-timeout})
                 (catch Exception e
                   {:exception e
                    :status :exception}))
              #(-> % :body (cheshire/parse-string true))
              retry-count))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;; P A G I N A T E D   Q U E R I E S
;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defn unpack-query-selection-results [r]
  (if (and (map? r) (contains? r :data))
    (let [data (get r :data)]
      (if (and (map? data) (= 1 (count data)))
        (first data)))))

(defn- valid-page-data? [x]
  (or (nil? x) (sequential? x)))

(defn run-paginated-query [graphql-doc selection-name page-size settings]
  (let [selection-key (keyword selection-name)]
    (loop [offset 0
           acc []
           id-set #{}]
      (let [parameters {selection-name {"offset" offset
                                        "limit" page-size}}
            _ (logging/debug "Page parameters" parameters)
            {:keys [status data] :as r}
            (-> graphql-doc
                (graphql/provide-query-selection-arguments parameters)
                graphql/render-compact
                (run-query settings))
            context {:offset offset
                     :acc acc
                     :id-set id-set
                     :run-query-response r}
            err (fn [e msg extra] (merge context extra
                                         {:status e
                                          :message msg}))]
        (logging/info "Query at offset" offset)
        (if (= :success status)
          (let [[ename page] (unpack-query-selection-results data)
                ids (map #(get % "id") page)
                seen-ids (filter id-set ids)]
            (cond
              (not= ename selection-key) (err :unexpected-selection-key "Unexpected selection key in response" {})
              (not (valid-page-data? page)) (err :invalid-page-data "Invalid page data" {:page page})
              (seq seen-ids) (err :ids-already-seen "Ids already seen" {})
              (empty? page) (let [result-data {:data {selection-key acc}}]
                              (assert (= [selection-key acc]
                                         (unpack-query-selection-results result-data)))
                              {:status :success :data result-data})
              :else (recur (+ offset page-size)
                           (into acc page)
                           (into id-set ids))))
          (err :run-query-failure "run-query failed" {}))))))

(defn run-parameterized-query [query {:keys [page-size version] :as settings}]
  {:pre [(or (nil? version) (string? version) (int? version))
         (or (nil? page-size) (int? page-size))]}
  (logging/debug "query" query)
  (logging/debug "version" version "page-size" page-size)
  (let [base-doc (graphql/provide-query-selection-arguments
                  (graphql/parse query)
                  {"concepts" {"version" version}
                   "concept_types" {"version" version}})
        selection-names (map #(.getName (:selection %))
                             (graphql/flatten-doc-selections base-doc))]
    
    ;; In case a page size is provided, we expect exactly one selection
    ;; of concepts.
    (assert (or (nil? page-size) (= selection-names ["concepts"])))
    
    (if (nil? page-size)

      ;; No need to paginate
      (run-query (graphql/render-compact base-doc) settings)

      (run-paginated-query base-doc "concepts" page-size settings))))

(defn first-successful-parameterized-query [query settings-to-try]
  {:pre [(seq settings-to-try)]}
  (loop [setting-result-pairs []
         [settings & remaining-settings] settings-to-try]
    (if (empty? settings)
      {:status :failures
       :settings-result-pairs setting-result-pairs}
      (let [result (run-parameterized-query query settings)]
        (if (= :success (:status result))
          (do
            (logging/info "Successful query")
            result)
          (do
            (logging/info "Failed query")
            (recur (conj setting-result-pairs [settings result])
                   remaining-settings)))))))

(comment

  (def sample-query "query MyQuery {
  concepts(preferred_label_contains: \"kare\") {
    id
    preferred_label
  }
}
")

  (def r0 (run-parameterized-query sample-query default-config))

  (def r1 (run-parameterized-query
           sample-query
           (merge default-config {:page-size 10
                                  :page-callback
                                  (fn [data] (println "Page callback at offset" (:offset data)))})))
  
  
  )
