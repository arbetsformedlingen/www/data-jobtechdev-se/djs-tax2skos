(ns tax2skos.dcat
  (:require [clojure.string :as cljstr]
            [clojure.java.io :as io]
            [tax2skos.utils :as utils]
            [clojure.tools.logging :as logging])
  (:import [se.ams.dcatprocessor Manager]
           [java.nio.file Path]
           [java.io File]
           [org.apache.commons.collections4.multimap ArrayListValuedHashMap]
           ))



;; The function below does the same as the expression
;;   (with-out-str (Application/convertFile (str dcat-filename)))
(defn convert-file [dcat-filename]
  {:pre [(string? dcat-filename)]}

  ;; The .createDcat method call will write to this file.
  (io/make-parents "build/debug/output.json")
  
  (let [manager (Manager.)
        path (Path/of dcat-filename (make-array String 0))
        api-spec-map (doto (ArrayListValuedHashMap.)
                       (.put (str path) (slurp dcat-filename)))]
    (io/make-parents dcat-filename)
    (.createDcat manager api-spec-map)))

(defn valid-dcat-result? [s]
  (cljstr/includes? s "<rdf:RDF"))

(defn valid-result-checker [s]
  (if (valid-dcat-result? s)
    []
    [{:message (utils/format-important "FAILED TO PRODUCE RESULT")}]))

(def suspicious-strings ["en¤" "sv¤"])

(defn get-context-string [s at]
  (let [marg 20
        n (count s)
        lower (max 0 (- at marg))
        upper (min n (+ at marg))]
    (transduce (remove nil?) str [(if (not= lower 0) "...")
                                  (utils/remove-line-breaks (subs s lower upper))
                                  (if (not= upper n) "...")])))

(defn suspicious-string-checker [s]
  (into []
        (comp (map (fn [ss]
                     (if-let [i (cljstr/index-of s ss)]
                       {:message (format "Suspicious string '%s' detected" ss)
                        :details (str "In this context: " (get-context-string s i))})))
              (remove nil?))
        suspicious-strings))

(defn list-dcat-issues [mangled-name full-dcat out-dir]
  {:pre [(string? mangled-name)
         (instance? File out-dir)]}
  (logging/debug "List dcat issues for" mangled-name (str out-dir))
  (let [dcat-filename (io/file out-dir "source" (str mangled-name ".json"))
        _ (io/make-parents dcat-filename)
        _ (->> full-dcat
               utils/json-string
               (spit dcat-filename))
        
        result (convert-file (str dcat-filename))

        debug-file (io/file out-dir "processed" (str mangled-name ".xml"))

        base-info {:mangled-name mangled-name
                   :dcat-filename dcat-filename
                   :debug-filename (str debug-file)}]
    
    (io/make-parents debug-file)
    (spit debug-file result)
    
    [base-info
     (into []
           (mapcat #(% result))
           [valid-result-checker
            suspicious-string-checker])]))
