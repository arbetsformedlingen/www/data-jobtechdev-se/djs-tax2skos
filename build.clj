(ns build
  (:require [clojure.tools.build.api :as b]))

(defn clean [_]
  (b/delete {:path "target"}))

(def basis (b/create-basis {:project "deps.edn"}))
(def class-dir "target/classes")
(def copy-src ["src/clj" "resources"])
(def uber-file "target/app.jar")

(defn ^:export uber [_]
  (clean nil)
  (b/copy-dir {:src-dirs copy-src
               :target-dir class-dir})
  (b/compile-clj {:basis basis
                  :src-dirs ["src"]
                  :ns-compile '[tax2skos.api]
                  :bindings {#'clojure.core/*warn-on-reflection* true}
                  :class-dir class-dir})
  (b/uber {:class-dir class-dir
           :uber-file uber-file
           :basis basis
           :main 'tax2skos.api}))
